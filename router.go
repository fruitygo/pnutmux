package pnutmux

import (
	"context"
	"embed"
	"errors"
	"fmt"
	"log/slog"
	"net/http"
	"os"
	"os/signal"
	"regexp"
	"strings"
	"sync"
	"syscall"
	"time"

	"github.com/redis/go-redis/v9"
	"gitlab.com/fruitygo/gojuice/juice/logjuice"
	"gitlab.com/fruitygo/gojuice/juice/workerjuice"
	"gitlab.com/fruitygo/pnutmux/contract"
	"gitlab.com/fruitygo/pnutmux/internal/adapter/util/limiter"
	log "gitlab.com/fruitygo/pnutmux/internal/adapter/util/logger"
	"gitlab.com/fruitygo/pnutmux/internal/adapter/util/redisclient"
	"gitlab.com/fruitygo/pnutmux/internal/adapter/util/responder"
	"gitlab.com/fruitygo/pnutmux/pnutconfig"
)

// PnutRouter represents a router for handling HTTP requests. It uses a sync.RWMutex for synchronization,
// stores the registered routes in a map[*regexp.Regexp]route, and has an http.HandlerFunc for handling not found routes.
//
// It holds the server instance in a contract.Server, uses a contract.Logger for logging, and a contract.Responder for responding.
type PnutRouter struct {
	pnutRouterMu    sync.RWMutex
	routes          map[*regexp.Regexp]*pnutconfig.Route
	notFoundHandler http.HandlerFunc
	srv             contract.Server
	logger          contract.Logger
	responder       contract.Responder
	limiter         contract.Limiter
	semaphore       workerjuice.SemaphoreInterface
	maxConcurrent   uint
	defaultLogger   bool
	defaultNotFound bool
	http2Config     *HTTP2Config
}

const (
	// Unlimited represents an unlimited request weight.
	Unlimited pnutconfig.RequestWeight = 0
)

// HTTP2Config contains TLS configuration information.
type HTTP2Config struct {
	UseH2C          bool     // Use H2C if true, otherwise use HTTPS.
	FS              embed.FS // File system for embedded files. Optional if UseH2C is true.
	CertificateFile string   // Path to the certificate file. Optional if UseH2C is true.
	KeyFile         string   // Path to the key file. Optional if UseH2C is true.
}

// IsValid checks if the HTTP2Config struct is valid.
func (h *HTTP2Config) IsValid() bool {
	if h.UseH2C {
		// If UseH2C is used, no further validation is needed.
		return true
	}

	return h.CertificateFile != "" && h.KeyFile != ""
}

// NewRouter creates and returns a new PnutRouter, implementing the contract.Router interface, with a specified semaphore count.
//
// An optional contract.Logger can be provided to customize logging behavior, otherwise a default logger implementation is used.
//
// A valid TLS configuration will enable HTTP2 if provided. If the HTTP2Config pointer is nil or struct contains zero values, HTTP2 will not be enabled.
func NewRouter(semCount uint, logger contract.Logger, http2Config *HTTP2Config) contract.Router {
	// Define a router.
	r := &PnutRouter{
		pnutRouterMu: sync.RWMutex{},
		routes:       make(map[*regexp.Regexp]*pnutconfig.Route),
		notFoundHandler: func(w http.ResponseWriter, r *http.Request) {
			responder := responder.NewAdapter()
			responder.WriteErrorResponse(w, http.StatusNotFound)
		},
		logger:        logger,
		responder:     responder.NewAdapter(),
		maxConcurrent: semCount,
	}

	// Create a semaphore with the specified count.
	r.semaphore = workerjuice.NewSemaphore(int(r.maxConcurrent))

	// Validate http/2 configuration.
	if http2Config != nil && http2Config.IsValid() {
		r.http2Config = http2Config
	}

	// Set defaultLogger flag to false if a custom logger is provided.
	r.defaultLogger = false

	// Create a default logger if none is provided.
	if r.logger == nil {
		r.logger = log.NewAdapter(os.Stdout, slog.LevelDebug, nil)

		// Set defaultLogger flag to true if a custom logger is not provided.
		r.defaultLogger = true
	}

	// Set defaultNotFound flag to true if a custom not found handler is not provided.
	r.defaultNotFound = true

	return r
}

// RateLimitingParams represents the parameters for configuring rate limiting.
//
// RedisClient is the Redis client to use for rate limiting.
//
// RedisTimeout is the timeout to use for Redis operations.
//
// MaxDailyScore is the maximum daily score to use for rate limiting.
//
// HeaderKey is the header key to use for rate limiting based on a custom header value, using this value as the identifier of the request.
//
// GroupID is the group ID to use to allow rate limiting of a user across multiple microservices.
//
// All parameters are required for rate limiting to be enabled, except for HeaderKey, which can be nil to use the IP-Based rate limiting.
//
// If any of the parameters are zero values, rate limiting will not be enabled.
type RateLimitingParams struct {
	RedisClient   *redis.Client
	RedisTimeout  time.Duration
	MaxDailyScore uint
	HeaderKey     *string
	GroupID       string
}

// NewRouterWithRedis creates and returns a new PnutRouter, implementing the contract.Router interface, with a specified semaphore count and optional Redis-Based rate limiting.
//
// An optional contract.Logger can be provided to customize logging behavior, and a Redis client, timeout, and maximum daily calls can be provided for rate limiting.
//
// If one of the parameters for rate limiting is not provided, the router will be created without rate limiting.
//
// A valid TLS configuration will enable HTTP2 if provided. If the HTTP2Config pointer is nil or struct contains zero values, HTTP2 will not be enabled.
func NewRouterWithRedis(semCount uint, logger contract.Logger, rp *RateLimitingParams, http2Config *HTTP2Config) contract.Router {
	r := NewRouter(semCount, logger, http2Config)

	// Preventive check for nil RateLimitingParams.
	if rp == nil {
		return r
	}

	// Preventive check for zero values.
	if rp.RedisClient == nil || rp.RedisTimeout == 0 || rp.MaxDailyScore == 0 || rp.GroupID == "" {
		return r
	}

	// Define a new Redis client adapter.
	clientAdapter := redisclient.NewRedisClientAdapter(rp.RedisClient)

	// Define a new PnutRouter.
	pnutRouter := r.(*PnutRouter)

	// Define a new rate limiter adapter.
	pnutRouter.limiter = limiter.NewAdapter(pnutRouter.logger, pnutRouter.responder, clientAdapter, rp.RedisTimeout, &pnutRouter.pnutRouterMu, pnutRouter.routes, rp.MaxDailyScore, rp.GroupID, rp.HeaderKey)

	return pnutRouter
}

// SetNotFoundHandler sets the custom not found handler for the PnutRouter.
func (r *PnutRouter) SetNotFoundHandler(handler http.HandlerFunc) {
	// Set the custom not found handler.
	r.notFoundHandler = handler

	// Set defaultNotFound flag to false if a custom not found handler is provided.
	r.defaultNotFound = false
}

// Run starts the HTTP server on the PnutRouter, listening for incoming requests on the provided address.
//
// Returns an error if the server is already running or encounters an error during execution.
func (r *PnutRouter) Run(addr string, idleTimeout, readTimeout, writeTimeout time.Duration) error {
	// Return an error if the server is already running.
	if r.srv != nil {
		err := "server is already running"
		return errors.New(err)
	}

	// Define the server with the wrapper.
	if r.http2Config != nil {
		var err error

		// Define the server with HTTP/2 support.
		r.srv, err = NewHTTP2ServerWrapper(addr, r, idleTimeout, readTimeout, writeTimeout, r.http2Config)
		if err != nil {
			return err
		}
	} else {
		// Define the server without HTTP/2 support.
		r.srv = NewServerWrapper(addr, r, idleTimeout, readTimeout, writeTimeout)
	}

	// OS related signals channel.
	sigChan := make(chan os.Signal, 1)

	// Send the interrupt signal to the sigChan in case of an interrupt or termination signal like Ctrl+C or SIGTERM.
	signal.Notify(sigChan, os.Interrupt, syscall.SIGTERM)

	// Error channel to handle errors from the server.
	errChan := make(chan error, 1)

	// Start the server in a goroutine.
	go func() {
		// Show informative chart.
		r.printRouterState()

		// Define the listen error.
		var listenErr error

		if r.http2Config != nil {
			// Start the server with HTTP/2 support if enabled.
			if r.http2Config.UseH2C {
				listenErr = r.srv.ListenAndServe()
			} else {
				listenErr = r.srv.ListenAndServeHTTP2()
			}
		} else {
			// Start the server without HTTP/2 support.
			listenErr = r.srv.ListenAndServe()
		}

		// Handle the listen error.
		if listenErr != nil {
			if listenErr != http.ErrServerClosed {
				r.logger.SLog(context.Background(), logjuice.Error, "", fmt.Sprintf("pnutmux failed to listen on %s: %v", addr, listenErr))
			} else {
				r.logger.SLog(context.Background(), logjuice.Info, "🥜", fmt.Sprintf("Pnutmux server closed on %s", addr))
			}
			errChan <- listenErr
		}
	}()

	// Launch a select statement to handle the OS signals and errors from the server.
	select {
	case <-sigChan:
		// Wait for the semaphore to be released before shutting down the server.
		r.semaphore.Wait()

		// Create a graceful shutdown context with a timeout of 30 seconds.
		ctx, cancel := context.WithTimeout(context.Background(), 30*time.Second)
		defer cancel()

		r.logger.SLog(context.Background(), logjuice.Info, "🛑", "Pnutmux stopping...")

		if err := r.srv.Shutdown(ctx); err != nil {
			return err
		}

		r.logger.SLog(context.Background(), logjuice.Info, "😴", "Pnutmux server has successfully shut down")

		// Reset the server instance to nil.
		r.srv = nil

		// If an error is received after the server is stopped by an interrupt signal, return the error.
		if err := <-errChan; err != nil {
			return err
		}

	case err := <-errChan:
		return err
	}

	// Unreachable code, because the select statement will always return before this point.
	return nil
}

// ServeHTTP handles incoming HTTP requests, allowing PnutRouter to implement the http.Handler interface.
func (r *PnutRouter) ServeHTTP(w http.ResponseWriter, req *http.Request) {
	// Acquire the semaphore before serving the request.
	r.semaphore.Acquire()

	r.pnutRouterMu.RLock()

	// Deferring the release of the semaphore and unlocking the read lock.
	defer func() {
		r.semaphore.Release()
		r.pnutRouterMu.RUnlock()
	}()

	// Define a slice to store the allowed methods for the route.
	var allowedMethods []string

	// Define the matching handler and regex.
	var matchingHandler http.Handler
	var matchingRegex *regexp.Regexp

	// Find the matching route for the request.
	for regex, route := range r.routes {
		if regex.MatchString(req.URL.Path) {
			if handler, ok := route.Handlers[req.Method]; ok {
				// Assign the matching handler and regex.
				matchingHandler = handler
				matchingRegex = regex

				// Define a slice to store the allowed methods for the current route.
				allowedMethods = make([]string, 0, len(route.Handlers))

				// Add the allowed methods to the slice.
				for method := range route.Handlers {
					allowedMethods = append(allowedMethods, method)
				}

				// Break the loop if the handler is found.
				break
			} else {
				// Prepare a slice of allowed methods to return with the 405 error.
				allowedMethods = make([]string, 0, len(route.Handlers))

				// Add the allowed methods to the slice.
				for method := range route.Handlers {
					allowedMethods = append(allowedMethods, method)
				}
			}
		}
	}

	// Convert the allowed methods slice to a comma-separated string.
	allowedMethodsStr := strings.Join(allowedMethods, ",")

	// Call the matching handler if it is not nil and the matching regex is not nil.
	if matchingHandler != nil && matchingRegex != nil {
		// Extract the parameters and query parameters from the request.
		p := extractParams(req, matchingRegex)
		qp := extractQueryParams(req)

		// Define a key for the request parameters.
		var serveHTTPKey requestParamsKey

		// Create a new context with the request parameters passed as a value.
		ctx := context.WithValue(req.Context(), serveHTTPKey, &requestParams{
			params:      p,
			queryParams: qp,
		})

		// Set the allowed methods in the response headers.
		w.Header().Set(pnutconfig.AccessControlAllowMethodsKey, allowedMethodsStr)
		w.Header().Set(pnutconfig.AllowKey, allowedMethodsStr)

		// Call the matching handler with the new context.
		matchingHandler.ServeHTTP(w, req.WithContext(ctx))

		return
	}

	// If matching handler was not called, return a 405 error with the allowed methods.
	if len(allowedMethods) > 0 {
		// Set the allowed methods in the response headers.
		w.Header().Set(pnutconfig.AccessControlAllowMethodsKey, allowedMethodsStr)
		w.Header().Set(pnutconfig.AllowKey, allowedMethodsStr)

		// Warn log for method not allowed.
		r.logger.SLog(context.Background(), logjuice.Warn, "", http.StatusText(http.StatusMethodNotAllowed))

		// Respond with a 405 error.
		r.responder.WriteErrorResponse(w, http.StatusMethodNotAllowed)

		return
	}

	// If any of the conditions above are not met, return a 404 error, meaning the route was not found.
	if r.notFoundHandler != nil {
		r.notFoundHandler.ServeHTTP(w, req)
	} else {
		// If a custom not found handler is not provided, respond with a raw 404 error.
		r.logger.SLog(context.Background(), logjuice.Warn, "", http.StatusText(http.StatusNotFound))

		r.responder.WriteErrorResponse(w, http.StatusNotFound)
	}
}

// Register registers a handler function for the specified regular expression and HTTP methods on the PnutRouter.
//
// If the router is not using Redis for rate limiting, the weight argument should be Unlimited, or 0.
func (r *PnutRouter) Register(reg *regexp.Regexp, handler http.HandlerFunc, weight pnutconfig.RequestWeight, methods ...string) error {
	r.pnutRouterMu.Lock()
	defer r.pnutRouterMu.Unlock()

	// Check if Redis client is nil when an attempt to register a route with a weight other than Unlimited is made.
	if weight != Unlimited && r.limiter == nil {
		errMsg := "request weight other than Unlimited can't be used if limiter is not enabled"
		return errors.New(errMsg)
	}

	// Prevent empty routes map.
	if r.routes == nil {
		r.routes = make(map[*regexp.Regexp]*pnutconfig.Route)
	}

	// Prevent empty methods slice.
	if len(methods) == 0 {
		methods = []string{http.MethodGet}
	}

	// Prevent duplicates of maps keys for both handlers and weights inside Route struct.
	// Since *regexp.Regexp will not match for a same regex pattern, we need to compare the string representation of the regex to get the proper Route struct.
	var existingRoute *pnutconfig.Route

	// Define a boolean to check if the route already exists.
	var exists bool

	// Compare the string representation of the regex to set the boolean flag to true if the route already exists.
	for k, v := range r.routes {
		if k.String() == reg.String() {
			existingRoute = v // This is a pointer to the Route struct.
			exists = true
			break
		}
	}

	// Check for duplicate entries in the existing route and return an error if found.
	if exists {
		if err := checkDuplicateEntries(existingRoute, methods, reg.String()); err != nil {
			return err
		}
	}

	// Create a new route to be registered.
	route := &pnutconfig.Route{
		Handlers: make(map[string]http.HandlerFunc),
		Weights:  make(map[string]pnutconfig.RequestWeight),
	}

	// Iterate over the methods slice and register the handler for each method.
	for _, method := range methods {
		// Validate the HTTP method.
		if !validHTTPMethod(method) {
			err := fmt.Errorf("invalid HTTP method: %s is not a valid HTTP method", method)

			return err
		}

		// Check route weight and engage the rate limiter if the weight is not Unlimited and greater than 0.
		if weight != Unlimited && weight > 0 {
			r.logger.SLog(context.Background(), logjuice.Info, "📫", fmt.Sprintf("Rate Limiting engaged for regex %s with method %s", reg.String(), method))

			// Engage the rate limiter for the handler by wrapping it with the limiter.
			route.Handlers[method] = r.limiter.Engage(handler)
		} else {
			r.logger.SLog(context.Background(), logjuice.Info, "📪", fmt.Sprintf("Rate Limiting NOT engaged for regex %s with method %s", reg.String(), method))

			// Set the handler directly if the weight is Unlimited or 0.
			route.Handlers[method] = handler
		}

		// Set the weight for the route.
		route.Weights[method] = weight
	}

	if exists {
		// Route with similar regex pattern exists, so we need to update the existing route.
		for method, handler := range route.Handlers {
			existingRoute.Handlers[method] = handler
		}

		for method, weight := range route.Weights {
			existingRoute.Weights[method] = weight
		}
	} else {
		// Route with the regex pattern does not exist, so we need to create a new route.
		r.routes[reg] = route
	}

	return nil
}

// Stop gracefully stops the HTTP server on the PnutRouter, returning an error if there is an issue shutting down or if the server is not running.
func (r *PnutRouter) Stop() error {
	r.pnutRouterMu.Lock()
	defer r.pnutRouterMu.Unlock()

	// Server is nil while attempting to stop it.
	if r.srv == nil {
		r.logger.SLog(context.Background(), logjuice.Error, "", "pnutmux server is not running while trying to stop it")

		return nil
	}

	r.logger.SLog(context.Background(), logjuice.Info, "🛑", "Pnutmux stopping...")

	if err := r.srv.Shutdown(context.Background()); err != nil {
		return err
	}

	r.logger.SLog(context.Background(), logjuice.Info, "😴", "Pnutmux server has successfully shut down")

	// Reset the server instance to nil.
	r.srv = nil

	return nil
}

// checkDuplicateEntries checks for duplicate entries in the existing route and returns an error if found.
func checkDuplicateEntries(existingRoute *pnutconfig.Route, methods []string, reg string) error {
	var errs []string

	for _, method := range methods {
		if _, ok := existingRoute.Handlers[method]; ok {
			err := fmt.Sprintf("regex %s is already mapped to method %s and a handler", reg, method)
			errs = append(errs, err)
		}

		if w, ok := existingRoute.Weights[method]; ok {
			err := fmt.Sprintf("regex %s is already mapped to method %s and weight %d", reg, method, w)
			errs = append(errs, err)
		}
	}

	// Return an error if there are duplicate entries.
	if len(errs) > 0 {
		return fmt.Errorf(strings.Join(errs, "\n"))
	}

	return nil
}
