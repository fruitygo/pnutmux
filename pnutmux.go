// Package pnutmux provides a routing module for HTTP requests.
//
// The pnutmux package offers functionality for handling routing in HTTP applications.
// It provides a flexible and customizable router that supports regular expression-Based
// URL pattern matching and allows the registration of handlers for different HTTP methods.
// The router can extract URL path parameters and query parameters, making them available
// to the registered handlers via the context.
//
// For more information and usage examples, refer to the README and documentation at
// https://gitlab.com/fruitygo/pnutmux.
package pnutmux

import (
	"context"
	"errors"
	"fmt"
	"io"
	"net/http"
	"os"
	"regexp"
	"strings"

	"gitlab.com/fruitygo/gojuice/juice/apijuice"
	"gitlab.com/fruitygo/pnutmux/pnutconfig"
)

const (
	IdPattern                  = `[0-9]+`                                         // Matches any sequence of digits, used for IDs.
	NamePattern                = `[a-zA-Z]+`                                      // Matches any sequence of letters, used for names.
	WordPattern                = `\w+`                                            // Matches any sequence of word characters (letters, digits, underscores).
	WordCaseInsensitivePattern = `(?i)\w+`                                        // Matches any sequence of word characters, case-insensitive.
	EmailPattern               = `[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}` // Matches any valid email address.
	MongoDBObjectIdPattern     = `[0-9a-fA-F]{24}`                                // Matches any valid MongoDB ObjectID.
	FilePatternWithDot         = `.*\.[^/]*$`                                     // Matches any file path with a file extension.
)

// Types.
type (
	requestParamsKey struct{}
)

// Aliases.
type (
	key = string
)

// RequestParams holds request parameters and query parameters in two map[string]string fields.
type requestParams struct {
	params      map[string]string
	queryParams map[string][]string
}

// extractParams uses a provided regular expression to extract parameters from a given HTTP request URL, returning them as a map[string]string.
//
// Returns an empty map if the regular expression is nil or no matches are found.
func extractParams(r *http.Request, re *regexp.Regexp) map[string]string {
	if re == nil {
		return make(map[string]string)
	}

	// Define parts.
	pathParts := strings.Split(r.URL.Path, "/")

	// Define endpoint.
	endpoint := strings.Trim(pathParts[1], "/")

	// Define matches.
	matches := re.FindStringSubmatch(r.URL.Path)
	if len(matches) == 0 {
		return make(map[string]string)
	}

	// Retrieve path parameter names.
	names := re.SubexpNames()

	// Build the map of path parameters.
	params := make(map[string]string)
	for i, name := range names {
		if i != 0 && name != "" && len(matches) > i && name != pnutconfig.SubpathKey {
			params[name] = matches[i]
		}
	}

	// Retrieve the endpoint name.
	params[pnutconfig.EndpointKey] = endpoint

	return params
}

// Vars returns a copy of the URL path parameters map from the context, excluding subpaths.
func Vars(ctx context.Context) map[string]string {
	var varsKey requestParamsKey

	params, ok := ctx.Value(varsKey).(*requestParams)
	if !ok {
		return nil
	}

	return params.params
}

// Var retrieves a specific URL path parameter from the context, excluding subpaths.
func Var(ctx context.Context, key string) string {
	var varKey requestParamsKey

	params, ok := ctx.Value(varKey).(*requestParams)
	if !ok {
		return ""
	}

	return params.params[key]
}

// extractQueryParams retrieves query parameters from a given HTTP request, returning them as a map[string][]string.
func extractQueryParams(r *http.Request) map[string][]string {
	queryParams := r.URL.Query()

	params := make(map[string][]string)

	for key, values := range queryParams {
		params[key] = values
	}

	return params
}

// QueryVars retrieves query variables from a given context, returning them as a map[string][]string or nil if the context lacks the necessary information.
func QueryVars(ctx context.Context) map[string][]string {
	var queryVarsKey requestParamsKey

	params, ok := ctx.Value(queryVarsKey).(*requestParams)
	if !ok {
		return nil
	}

	return params.queryParams
}

// QueryVar retrieves the value of a specified query variable from a given context, returning an empty string if the context lacks the necessary information or the key is not found.
func QueryVar(ctx context.Context, key string) []string {
	var queryVarKey requestParamsKey

	params, ok := ctx.Value(queryVarKey).(*requestParams)
	if !ok {
		return []string{}
	}

	return params.queryParams[key]
}

// validHTTPMethod checks the validity of a HTTP method string.
func validHTTPMethod(method string) bool {
	switch method {
	case http.MethodGet, http.MethodPost, http.MethodPut, http.MethodDelete, http.MethodHead, http.MethodTrace, http.MethodOptions, http.MethodPatch, http.MethodConnect:
		return true
	default:
		return false
	}
}

// CompileRegex compiles a regular expression pattern from a provided endpoint and key-value parameter pairs, returning the compiled pattern and any compilation errors.
//
// To set a parameter as a subpath, use the key pnutmux.Subpath.
//
// In order to create a endpoint at the root, use an empty string as the endpoint or use forward slash "/".
//
// To define your pairs of key -> value parameters, provide them as a list of strings, where the first string is the key and the second string is the value. A check is performed to ensure that the number of parameters is even, since they are pairs of key -> values.
//
// Do not include any regex anchors for the values, as they are added automatically. Adding them will lead to unexpected results.
//
// Here is an example of how to use this function to create a regex pattern for a URL endpoint:
//
// /jobs/{job_id}/applications/{application_id}
//
// CompileRegex("/jobs", "job_id", pnutmux.IdPattern, "applications", pnutmux.Subpath, "application_id", pnutmux.IdPattern)
func CompileRegex(endpoint string, pairs ...string) (*regexp.Regexp, error) {
	// Define an invalid pattern.
	invalidPattern := regexp.MustCompile(`^[^/]*\/[^/]*\/|[^a-zA-Z0-9/_-]`)

	// Check if the endpoint contains invalid characters.
	if invalidPattern.MatchString(endpoint) {
		err := "endpoint contains invalid characters"

		return nil, errors.New(err)
	}

	// If the endpoint is empty, set it to the root URL path.
	if endpoint == "" {
		endpoint = "/"
	}

	// Verify that the endpoint does not contain regex parameters if it is the root URL path.
	if endpoint == "/" && len(pairs) > 0 {
		err := "invalid input, providing path parameters when endpoint is root is prohibited"

		return nil, errors.New(err)
	}

	// Verify that the number of parameters is even, since they are pairs of key -> values.
	if len(pairs)%2 != 0 {
		err := "invalid number of parameters, an even number of strings must be provided"

		return nil, errors.New(err)
	}

	// Create a map to store unique keys.
	keys := make(map[string]bool)

	// Create the regex pattern by concatenating the endpoint and regex parameters.
	pattern := fmt.Sprintf(`^%s`, regexp.QuoteMeta(endpoint))

	// Loop trough key & value pairs.
	for i := 0; i < len(pairs); i += 2 {
		id := pairs[i]

		// Verify that the key is unique, unless it is "subpath".
		if id != pnutconfig.SubpathKey {
			if _, ok := keys[id]; ok {
				err := fmt.Sprintf("duplicate key %s in the regex parameters", id)

				return nil, errors.New(err)
			}
		}

		// Mark the current key as processed by adding it to the map.
		keys[id] = true

		// Extract the value associated with the current key.
		rid := pairs[i+1]

		// Validate that the value is a valid regex pattern (the return value is omitted).
		regexp.MustCompile(regexp.QuoteMeta(rid))

		// Construct a named capture group pattern using the key and the value.
		pairPattern := fmt.Sprintf(`/(?P<%s>%s)`, id, rid)

		// Append the constructed pattern to the overall regex pattern.
		pattern += pairPattern
	}

	// Add anchor.
	pattern += "$"

	return regexp.Compile(pattern)
}

// SetAllowedOrigins sets the ALLOWED_ORIGINS environment variable to a provided, comma-separated list of allowed origins for CORS, logging and returning any errors encountered.
func SetAllowedOrigin(origins string) error {
	return setAllowedOriginWithName(origins, pnutconfig.AllowedOriginsKey)
}

func setAllowedOriginWithName(origins, name string) error {
	if err := os.Setenv(name, origins); err != nil {
		err = fmt.Errorf("failed to set %s environment variable: %w", name, err)

		return err
	}

	return nil
}

// Respond writes a JSON response to the provided http.ResponseWriter with the given data and HTTP status code.
//
// It utilizes the apijuice.WriteJSONResponse function for writing the response.
//
// The response is not compressed, use pnutmux.Gzip() middleware to enable compression.
//
// Parameters:
//   - w: http.ResponseWriter to write the response to.
//   - data: Data to be serialized and sent as the response payload.
//   - code: HTTP status code to be set in the response.
func Respond(w http.ResponseWriter, data interface{}, code int, escapeHTML bool) {
	if err := apijuice.WriteJSONResponse(w, data, code, escapeHTML); err != nil {
		apijuice.WriteErrorResponse(w, http.StatusInternalServerError, false)

		return
	}
}

// RespondError writes an error response to the provided http.ResponseWriter with the given error message and HTTP status code.
//
// It utilizes the apijuice.WriteErrorResponse function for writing the error response.
//
// The response message is extracted from the error code with http.StatusText.
//
// Parameters:
//   - w: http.ResponseWriter to write the error response to.
//   - code: HTTP status code to be set in the response.
func RespondErr(w http.ResponseWriter, code int) {
	apijuice.WriteErrorResponse(w, code, false)
}

// ServeHTML writes an HTML response to the provided http.ResponseWriter with the given HTML content and HTTP status code.
//
// Parameters:
//   - w: http.ResponseWriter to write the response to.
//   - htmlContent: HTML content to be sent as the response payload.
//   - code: HTTP status code to be set in the response.
func ServeHTML(w http.ResponseWriter, htmlContent io.Reader, code int) {
	apijuice.ServeHTML(w, htmlContent, code)
}
