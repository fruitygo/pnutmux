package pnutmux

import (
	"context"
	"net/http"
	"testing"
	"time"

	"gitlab.com/fruitygo/gojuice/juice/testjuice"
)

func TestServerwrapper(t *testing.T) {
	// Create a new serverwrapper instance.
	serverwrapper := NewServerWrapper(":8080", http.DefaultServeMux, time.Second, time.Second, time.Second)

	// Start the server in a separate goroutine.
	go func() {
		err := serverwrapper.ListenAndServe()
		if err != nil && err != http.ErrServerClosed {
			t.Errorf("failed to start server: %v", err)
		}
	}()

	// Wait for the server to start.
	if err := testjuice.WaitForServer(localhost8080URL, waitForServerDelay); err != nil {
		t.Errorf("failed to wait for the server to start: %v", err)
	}

	// Call the Shutdown method.
	err := serverwrapper.Shutdown(context.Background())
	// Assert the expected behavior.
	if err != nil {
		t.Errorf("unexpected error on Shutdown, %v", err)
	}
}

func TestServerWrapperWithH2C(t *testing.T) {
	// Create a new serverwrapper instance with H2C.
	serverwrapper, err := NewHTTP2ServerWrapper(":8080", http.DefaultServeMux, time.Second, time.Second, time.Second, &HTTP2Config{UseH2C: true})
	testjuice.CheckErrorAndFailWithMessage(t, err, "unexpected error on NewHTTP2ServerWrapper")

	// Start the server in a separate goroutine.
	go func() {
		err := serverwrapper.ListenAndServe()
		if err != nil && err != http.ErrServerClosed {
			t.Errorf("failed to start server: %v", err)
		}
	}()

	// Wait for the server to start.
	if err := testjuice.WaitForServer(localhost8080URL, waitForServerDelay); err != nil {
		t.Errorf("failed to wait for the server to start: %v", err)
	}

	// Call the Shutdown method.
	err = serverwrapper.Shutdown(context.Background())
	// Assert the expected behavior.
	if err != nil {
		t.Errorf("unexpected error on Shutdown, %v", err)
	}
}

func TestServerwrapperWithTLS(t *testing.T) {
	// Define test cases.
	testCases := []struct {
		certificate string
		key         string
		expectError bool
	}{
		{"embed/test_server.crt", "embed/test_server.key", false},
		{"embed/bad_server.crt", "embed/test_server.key", true},
		{"embed/test_server.crt", "embed/bad_server.key", true},
		{"embed/bad_server.crt", "embed/very_bad_server.key", true},
		{"embed/very_bad_server.crt", "embed/bad_server.key", true},
	}

	// Iterate test cases.
	for _, tc := range testCases {
		http2Config := &HTTP2Config{
			FS:              testEmbed,
			CertificateFile: tc.certificate,
			KeyFile:         tc.key,
		}

		// Create a new serverwrapper instance with TLS.
		serverwrapper, err := NewHTTP2ServerWrapper(":8080", http.DefaultServeMux, time.Second, time.Second, time.Second, http2Config)
		if err != nil {
			if !tc.expectError {
				t.Errorf("unexpected error on NewHTTP2ServerWrapper, %v", err)
			}

			continue
		}

		// Start the server in a separate goroutine.
		go func() {
			err := serverwrapper.ListenAndServeHTTP2()
			if err != nil && err != http.ErrServerClosed {
				t.Errorf("failed to start server: %v", err)
			}
		}()

		// Wait for the server to start.
		if err := testjuice.WaitForServer(localhost8080URL, waitForServerDelay); err != nil {
			t.Errorf("failed to wait for the server to start: %v", err)
		}

		// Call the Shutdown method.
		err = serverwrapper.Shutdown(context.Background())
		if err != nil {
			t.Errorf("unexpected error on Shutdown, %v", err)
		}
	}
}

func TestServerwrapper_NilServer(t *testing.T) {
	// Create a serverwrapper instance with nil as *http.Server.
	serverwrapper := &serverwrapper{}

	// Try to start the server.
	err := serverwrapper.ListenAndServe()

	// Assert the expected behavior.
	if err == nil || err.Error() != "server is nil while trying to listen and serve" {
		t.Errorf("expected error 'server is nil while trying to listen and serve', got %v", err)
	}
}

func TestServerwrapper_NilServerTLS(t *testing.T) {
	// Create a serverwrapper instance with nil as *http.Server.
	serverwrapper := &serverwrapper{}

	// Try to start the server.
	err := serverwrapper.ListenAndServeHTTP2()

	// Assert the expected behavior.
	if err == nil || err.Error() != "server is nil while trying to listen and serve" {
		t.Errorf("expected error 'server is nil while trying to listen and serve', got %v", err)
	}
}
