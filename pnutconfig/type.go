package pnutconfig

import (
	"net/http"
)

// Key is a string alias to identify various keys in Pnutmux.
type Key = string

// RequestWeight represents the weight of a request. It is used to limit the rate of requests.
type RequestWeight uint

// Route represents a route in the application.
//
// It contains a map of handlers for each HTTP method,
// and a map of weights for each HTTP method.
//
// Handlers holds the http.HandlerFunc for each HTTP method.
//
// Weights holds the RequestWeight for each HTTP method.
type Route struct {
	Handlers map[string]http.HandlerFunc
	Weights  map[string]RequestWeight
}
