package pnutconfig

import "time"

const (
	OneDayInMs float64 = float64(24 * time.Hour / time.Millisecond)
	// V          string  = "v1.15.9"

	AccessControlAllowMethodsKey Key = "Access-Control-Allow-Methods"
	AccessControlAllowHeadersKey Key = "Access-Control-Allow-Headers"
	AccessControlAllowOriginKey  Key = "Access-Control-Allow-Origin"
	CFConnectingIPKey            Key = "CF-Connecting-IP"
	AllowedOriginsKey            Key = "ALLOWED_ORIGINS"
	XForwardedForKey             Key = "X-Forwarded-For"
	XRealIPKey                   Key = "X-Real-IP"
	EndpointKey                  Key = "endpoint"
	SubpathKey                   Key = "subpath"
	AllowKey                     Key = "Allow"
)
