package pnutconfig

import "crypto/tls"

// GetHTTP2Config returns a new tls.Config instance.
//
// It uses a minimum TLS version of 1.2, and supports HTTP/2 and HTTP/1.1.
func GetHTTP2Config() *tls.Config {
	return &tls.Config{
		Rand:               nil,
		Time:               nil,
		InsecureSkipVerify: false,
		MinVersion:         tls.VersionTLS12,
		ClientAuth:         tls.NoClientCert,
		NextProtos:         []string{"h2", "http/1.1"},
	}
}
