package pnutmux

import (
	"bytes"
	"compress/gzip"
	"errors"
	"io"
	"log/slog"
	"net/http"
	"net/http/httptest"
	"os"
	"testing"

	"gitlab.com/fruitygo/pnutmux/internal/adapter/util/logger"
	"gitlab.com/fruitygo/pnutmux/pnutconfig"
)

func TestChain(t *testing.T) {
	// Create a test handler that will be wrapped by the middleware chain.
	testHandler := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(http.StatusOK)

		w.Write([]byte("Test Handler"))
	})

	// Create the middleware functions for the test.
	middleware1 := func(next http.HandlerFunc) http.HandlerFunc {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			// Perform some middleware logic before calling the next handler.
			w.Header().Set("X-Middleware-1", "true")

			next.ServeHTTP(w, r)
		})
	}

	middleware2 := func(next http.HandlerFunc) http.HandlerFunc {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			// Perform some middleware logic before calling the next handler.
			w.Header().Set("X-Middleware-2", "true")

			next.ServeHTTP(w, r)
		})
	}

	// Wrap the test handler with the middleware chain.
	wrappedHandler := Chain(middleware1, middleware2)(testHandler)

	// Create a new request.
	req, err := http.NewRequest("GET", "/test", nil)
	if err != nil {
		t.Fatalf("Failed to create test request: %v", err)
	}

	// Create a response recorder to capture the response.
	rr := httptest.NewRecorder()

	// Serve the wrapped handler directly.
	wrappedHandler.ServeHTTP(rr, req)

	// Check the response status code.
	if rr.Code != http.StatusOK {
		t.Errorf("expected status code %d, but got %d", http.StatusOK, rr.Code)
	}

	// Check the response headers set by the middleware.
	expectedHeader1 := "true"
	if header1 := rr.Header().Get("X-Middleware-1"); header1 != expectedHeader1 {
		t.Errorf("expected header X-Middleware-1 to be %s, but got %s", expectedHeader1, header1)
	}

	expectedHeader2 := "true"
	if header2 := rr.Header().Get("X-Middleware-2"); header2 != expectedHeader2 {
		t.Errorf("expected header X-Middleware-2 to be %s, but got %s", expectedHeader2, header2)
	}

	// Check the response body.
	expectedBody := "Test Handler"
	if body := rr.Body.String(); body != expectedBody {
		t.Errorf("expected response body to be %s, but got %s", expectedBody, body)
	}
}

// TestCors is a unit test function that tests the CORS middleware.
// It sets up a test handler to be wrapped by the CORS middleware.
// Then, it creates a new request and recorder for testing.
// The "ALLOWED_ORIGINS" environment variable is set to "http://example.com" for the test.
// The CORS middleware instance is created by calling the CORS function with the test handler as an argument.
// The request is served through the CORS middleware, and the response is recorded.
// The test asserts the expected value of the "Access-Control-Allow-Origin" header in the response,
// comparing it with the value set in the "ALLOWED_ORIGINS" environment variable.
// It also asserts the expected response status code and body.
func TestCors(t *testing.T) {
	// Set up the test handler to be wrapped by the cors middleware.
	handler := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(http.StatusOK)
		w.Write([]byte("Hello, World!"))
	})

	// Define test cases.
	tests := []struct {
		name           string
		origin         string
		allowedOrigins string
		expectedOrigin string
		expectedStatus int
		httpMethod     string
	}{
		{
			name:           "Allow any origin",
			origin:         "http://example.com",
			allowedOrigins: "",
			expectedOrigin: "*",
			expectedStatus: http.StatusOK,
			httpMethod:     http.MethodGet,
		},
		{
			name:           "Allow specific origin",
			origin:         "http://example.com",
			allowedOrigins: "http://example.com,http://localhost:3000",
			expectedOrigin: "http://example.com",
			expectedStatus: http.StatusOK,
			httpMethod:     http.MethodGet,
		},
		{
			name:           "Disallow origin",
			origin:         "http://notallowed.com",
			allowedOrigins: "http://example.com,http://localhost:3000",
			expectedOrigin: "",
			expectedStatus: http.StatusOK,
			httpMethod:     http.MethodGet,
		},
	}

	// Iterate test cases.
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			// Create initial preflight request to set the Access-Control-Allow-Methods header
			preflightReq := httptest.NewRequest(http.MethodOptions, "/", nil)
			preflightReq.Header.Set("Origin", tt.origin)
			preflightRec := httptest.NewRecorder()

			// Set the ALLOWED_ORIGINS environment variable for the test.
			os.Setenv(pnutconfig.AllowedOriginsKey, tt.allowedOrigins)

			// Create a new cors middleware instance and wrap the test handler.
			corsHandler := cors(handler)

			// Serve the preflight request through the cors middleware.
			corsHandler.ServeHTTP(preflightRec, preflightReq)

			// Assert the expected Access-Control-Allow-Origin header value for the preflight request.
			actualPreflightOrigin := preflightRec.Header().Get(pnutconfig.AccessControlAllowOriginKey)
			if actualPreflightOrigin != tt.expectedOrigin {
				t.Errorf("enexpected Access-Control-Allow-Origin header value for preflight request. expected: %s, got: %s", tt.expectedOrigin, actualPreflightOrigin)
			}

			// Assert the expected Access-Control-Allow-Methods header value for the preflight request.
			actualAllowedMethods := preflightRec.Header().Get(pnutconfig.AccessControlAllowMethodsKey)
			expectedAllowedMethods := "*"
			if actualAllowedMethods != expectedAllowedMethods {
				t.Errorf("unexpected Access-Control-Allow-Methods header value for preflight request. expected: %s, got: %s", expectedAllowedMethods, actualAllowedMethods)
			}

			// Create a new request to be handled by the cors middleware.
			req := httptest.NewRequest(tt.httpMethod, "/", nil)
			req.Header.Set("Origin", tt.origin)

			// Define a recorder.
			rec := httptest.NewRecorder()

			// Serve the actual request through the cors middleware.
			corsHandler.ServeHTTP(rec, req)

			// Retrieve the response from the recorder.
			resp := rec.Result()

			// Assert the expected Access-Control-Allow-Origin header value for the actual request.
			actualOrigin := resp.Header.Get(pnutconfig.AccessControlAllowOriginKey)
			if actualOrigin != tt.expectedOrigin {
				t.Errorf("unexpected Access-Control-Allow-Origin header value for actual request. expected: %s, got: %s", tt.expectedOrigin, actualOrigin)
			}

			// Assert the expected response status code.
			actualStatus := resp.StatusCode
			if actualStatus != tt.expectedStatus {
				t.Errorf("unexpected status code. expected: %d, got: %d", tt.expectedStatus, actualStatus)
			}

			// Assert the expected response body.
			expectedBody := "Hello, World!"
			actualBody := rec.Body.String()
			if actualBody != expectedBody {
				t.Errorf("unexpected response body. expected: %s, got: %s", expectedBody, actualBody)
			}
		})
	}
}

func TestGzipMiddleware(t *testing.T) {
	// Set up the test handler to be wrapped by the gzip middleware.
	handler := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(http.StatusOK)
		w.Write([]byte("Hello, World!"))
	})

	// Define test cases.
	testCases := []struct {
		name               string
		shouldAcceptGzip   bool
		expectedEncoding   string
		expectedStatusCode int
		expectedBody       string
	}{
		{
			name:               "With Gzip Accept-Encoding",
			shouldAcceptGzip:   true,
			expectedEncoding:   "gzip",
			expectedStatusCode: http.StatusOK,
			expectedBody:       "Hello, World!",
		},
		{
			name:               "Without Gzip Accept-Encoding",
			shouldAcceptGzip:   false,
			expectedEncoding:   "",
			expectedStatusCode: http.StatusOK,
			expectedBody:       "Hello, World!",
		},
	}

	// Iterate test cases.
	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			// Create a new request to be handled by the gzip middleware.
			req := httptest.NewRequest(http.MethodGet, "/", nil)
			if tc.shouldAcceptGzip {
				req.Header.Set("Accept-Encoding", "gzip")
			}

			// Define the recorder.
			rec := httptest.NewRecorder()

			// Create a new gzip middleware instance and wrap the test handler.
			gzipHandler := Gzip(handler)

			// Serve the request through the gzip middleware.
			gzipHandler.ServeHTTP(rec, req)

			// Retrieve the response from the recorder.
			resp := rec.Result()

			// Assert the expected Content-Encoding header value.
			actualEncoding := resp.Header.Get("Content-Encoding")
			if actualEncoding != tc.expectedEncoding {
				t.Errorf("unexpected Content-Encoding header value. expected: %s, got: %s", tc.expectedEncoding, actualEncoding)
			}

			// Assert the expected response status code.
			actualStatus := resp.StatusCode
			if actualStatus != tc.expectedStatusCode {
				t.Errorf("unexpected status code. expected: %d, got: %d", tc.expectedStatusCode, actualStatus)
			}

			// Define a string to hold the body of the response.
			var actualBody string

			if tc.shouldAcceptGzip {
				// Define a reader.
				reader, err := gzip.NewReader(rec.Body)
				if err != nil {
					t.Fatal(err)
				}

				// Defer body closure.
				defer reader.Close()

				// Read the body.
				bodyBytes, err := io.ReadAll(reader)
				if err != nil {
					t.Fatal(err)
				}

				// Convert the body to a string.
				actualBody = string(bodyBytes)
			} else {
				actualBody = rec.Body.String()
			}

			// Assert.
			if actualBody != tc.expectedBody {
				t.Errorf("unexpected response body. expected: %s, got: %s", tc.expectedBody, actualBody)
			}
		})
	}
}

func TestWrite(t *testing.T) {
	// Define a slice of bytes.
	bytes := []byte("test")

	// Define a logger.
	logger := logger.NewAdapter(os.Stdout, slog.LevelInfo, nil)

	// Define a wrapped response writer.
	wr := NewWrappedResponseWriter(logger, httptest.NewRecorder())

	// Remove compression.
	wr.gw = nil

	// Write.
	_, err := wr.Write(bytes)
	if err != nil {
		t.Errorf("unexpected error: %v", err)
	}

	// Assert.
	rr, ok := wr.rw.(*httptest.ResponseRecorder)
	if !ok {
		t.Error("failed to assert wr.rw to *httptest.ResponseRecorder")
	}

	// Retrieve body as a string.
	body := rr.Body.String()

	// Assert.
	if body != string(bytes) {
		t.Errorf("unexpected body. expected: %v, got: %v", bytes, body)
	}
}

// Mock gzip writer that returns an error on Write
type errorWriter struct{}

// Write returns an error.
func (ew *errorWriter) Write(p []byte) (n int, err error) {
	return 0, errors.New("test error")
}

func TestFlushError(t *testing.T) {
	// Create a buffer to capture log output.
	var buf bytes.Buffer

	// Create a gzip writer with an errorWriter.
	gw, err := gzip.NewWriterLevel(&errorWriter{}, gzip.BestCompression)
	if err != nil {
		t.Fatalf("failed to create gzip writer: %v", err)
	}

	// Define a logger.
	logger := logger.NewAdapter(&buf, slog.LevelInfo, nil)

	// Define a wrapped response writer.
	wr := NewWrappedResponseWriter(logger, httptest.NewRecorder())

	// Assign the gzip writer to the wrapped response writer.
	wr.gw = gw

	// Flush the gzip writer.
	wr.Flush()

	// Verify output.
	if buf.Len() == 0 {
		t.Errorf("expected log output, got none")
	}
}
