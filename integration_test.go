// NOTE: GOMAXPROCS environment variable should be increased accordingly if integration_test fails.
// This variable simulates an (almost) instantaneous load of http requests to localhost:8000.
package pnutmux

import (
	"fmt"
	"io"
	"log"
	"net/http"
	"strings"
	"sync"
	"sync/atomic"
	"testing"
	"time"

	"gitlab.com/fruitygo/gojuice/juice/testjuice"
)

// Variable used in the last concurrent test.
const (
	concurrentRequests = 20
	waitForServerDelay = 3 * time.Second
	localhost8000URL   = "http://localhost:8000"
	localhost8080URL   = "http://localhost:8080"
)

// TestOverallWithSingleParameter tests the overall behavior of the router when handling requests with single parameters.
// It creates a router, compiles a regular expression from the endpoint and parameter pairs, registers a handler function
// for the compiled regular expression, starts the server, sends an HTTP request to the specified URL and method,
// verifies the response body against the expected value, stops the server, and waits for the server to shut down completely.
func TestOverallWithSingleParameter(t *testing.T) {
	// Define test cases.
	tests := []struct {
		testName string
		endpoint string
		pairs    []string
		url      string
		method   string
		expected string
	}{
		{
			testName: "Root path test, no parameters",
			endpoint: "/",
			pairs:    []string{},
			url:      "http://localhost:8000/",
			method:   http.MethodGet,
			expected: "Welcome! The url parameter is: ",
		},
		{
			testName: "Users path test, id parameters",
			endpoint: "/users",
			pairs:    []string{"id", `[0-9]\d*`},
			url:      "http://localhost:8000/users/1",
			method:   http.MethodGet,
			expected: "Welcome! The url parameter is: 1",
		},
		{
			testName: "Products path test, no parameters",
			endpoint: "/products",
			pairs:    []string{"id", `[0-9]\d*`},
			url:      "http://localhost:8000/products/044",
			method:   http.MethodGet,
			expected: "Welcome! The url parameter is: 044",
		},
		{
			testName: "Products path test, multiple subpaths to be ignored",
			endpoint: "/products",
			pairs:    []string{"id", `[0-9]\d*`, Subpath(), "category", "category_id", `[0-9]\d*`, Subpath(), "product", "product_id", `[0-9]\d*`},
			url:      "http://localhost:8000/products/44/category/77/product/1",
			method:   http.MethodGet,
			expected: "Welcome! The url parameter is: 44",
		},
	}

	// Iterate test cases.
	for _, tt := range tests {
		t.Run(tt.testName, func(t *testing.T) {
			// Instanciate the router.
			r := NewRouter(semCount, nil, nil)

			// Define the regex.
			regex, err := CompileRegex(tt.endpoint, tt.pairs...)
			if err != nil {
				t.Errorf("CompileRegex failed: %v", err)
			}

			// Define the handler.
			handler := func(w http.ResponseWriter, r *http.Request) {
				// Retrieve variables.
				allVariables := Vars(r.Context())

				// Retrieve variable 1.
				variable1 := Var(r.Context(), "id")

				// Retrieve expected variable.
				variable1expected := allVariables["id"]

				// Verify if both variables are equal.
				if allVariables["id"] != variable1 {
					t.Errorf("error comparing url variables, expected %s, got %s", variable1expected, variable1)
				}

				// Write response.
				fmt.Fprintf(w, "Welcome! The url parameter is: %s", variable1)
			}

			// Register the route.
			r.Register(regex, handler, Unlimited, tt.method)

			// Create a channel to signal when the server has been stopped.
			done := make(chan bool)

			// Start the server in the background.
			go func() {
				// Run.
				err := r.Run("localhost:8000", 120*time.Second, 5*time.Second, 5*time.Second)
				if err != nil && err != http.ErrServerClosed {
					t.Errorf("failed to start the server on localhost:8000: %v", err)
				} else {
					done <- true
				}
			}()

			// Wait for the server to start.
			if err := testjuice.WaitForServer(localhost8000URL, waitForServerDelay); err != nil {
				t.Errorf("failed to wait for the server to start: %v", err)
			}

			// Make a request to the server.
			resp, err := http.Get(tt.url)
			if err != nil {
				t.Errorf("failed to make request: %v", err)
			}

			// Defer body closure.
			defer resp.Body.Close()

			// Read the response body.
			body, err := io.ReadAll(resp.Body)
			if err != nil {
				t.Errorf("failed to read response body: %v", err)
			}

			// Compare the response to the expected value.
			if string(body) != tt.expected {
				t.Errorf("unexpected response, expected %s, got %s", tt.expected, string(body))
			}

			// Stop the server.
			if err := r.Stop(); err != nil {
				t.Errorf("failed to stop the server: %v", err)
			}

			// Wait for the server to shut down completely.
			<-done
			done = nil
		})
	}
}

// TestOverallWithMultipleParameters tests the overall behavior of the router when handling requests with multiple parameters.
// It creates a router, compiles a regular expression from the endpoint and parameter pairs, registers a handler function
// for the compiled regular expression, starts the server, sends an HTTP request to the specified URL and method,
// verifies the response body against the expected value, stops the server, and waits for the server to shut down completely.
func TestOverallWithMultipleParameters(t *testing.T) {
	// Define test cases.
	tests := []struct {
		testName string
		endpoint string
		pairs    []string
		url      string
		method   string
		expected string
	}{
		{
			testName: "Users path test",
			endpoint: "/users",
			pairs:    []string{"id", `[1-9]\d*`, "category", `[0-9]\d*`},
			url:      "http://localhost:8000/users/1/3",
			method:   http.MethodGet,
			expected: "Welcome! The url parameter is: 1 & 3",
		},
		{
			testName: "Inner subpath test",
			endpoint: "/users",
			pairs:    []string{"id", IdPattern, "category", "category", "category_id", IdPattern},
			url:      "http://localhost:8000/users/1/category/55",
			method:   http.MethodGet,
			expected: "Welcome! The url parameter is: 1 & category",
		},
		{
			testName: "Pre-defined Regex Test",
			endpoint: "/users",
			pairs:    []string{"id", IdPattern, "category", WordCaseInsensitivePattern},
			url:      "http://localhost:8000/users/1/ThisIsAPascalCasePattern",
			method:   http.MethodGet,
			expected: "Welcome! The url parameter is: 1 & ThisIsAPascalCasePattern",
		},
		{
			testName: "Products path test",
			endpoint: "/products",
			pairs:    []string{"id", `[1-9]\d*`, "category", `[0-9]\d*`},
			url:      "http://localhost:8000/products/9/5",
			method:   http.MethodGet,
			expected: "Welcome! The url parameter is: 9 & 5",
		},
	}

	// Iterate test cases.
	for _, tt := range tests {
		t.Run(tt.testName, func(t *testing.T) {
			// Instantiate the router
			r := NewRouter(semCount, nil, nil)

			// Define the regex.
			regex, err := CompileRegex(tt.endpoint, tt.pairs...)
			if err != nil {
				t.Errorf("CompileRegex failed: %v", err)
			}

			// Define the handler.
			handler := func(w http.ResponseWriter, r *http.Request) {
				// Retrieve variables.
				allVariables := Vars(r.Context())

				// Retrieve variable 1.
				variable1 := Var(r.Context(), "id")

				// Retrieve variable 2.
				variable2 := allVariables["category"]

				// Define expected variable.
				variable1expected := allVariables["id"]

				// Verify if variables are the same.
				if allVariables["id"] != variable1 {
					t.Errorf("error comparing url variables, expected %s, got %s", variable1expected, variable1)
				}

				// Write the response.
				fmt.Fprintf(w, "Welcome! The url parameter is: %s & %s", variable1, variable2)
			}

			// Register the route.
			r.Register(regex, handler, Unlimited, tt.method)

			// Create a channel to signal when the server has been stopped.
			done := make(chan bool)

			// Start the server in the background.
			go func() {
				// Run.
				err := r.Run("localhost:8000", 120*time.Second, 5*time.Second, 5*time.Second)
				if err != nil && err != http.ErrServerClosed {
					t.Errorf("Failed to start the server on localhost:8000: %v", err)
				} else {
					done <- true
				}
			}()

			// Wait for the server to start.
			if err := testjuice.WaitForServer(localhost8000URL, waitForServerDelay); err != nil {
				t.Errorf("failed to wait for the server to start: %v", err)
			}

			// Make a request to the server.
			resp, err := http.Get(tt.url)
			if err != nil {
				t.Errorf("Failed to make request: %v", err)
			}

			// Defer body closure.
			defer resp.Body.Close()

			// Read the response body.
			body, err := io.ReadAll(resp.Body)
			if err != nil {
				t.Errorf("Failed to read response body: %v", err)
			}

			// Compare the response to the expected value.
			if string(body) != tt.expected {
				t.Errorf("Unexpected response, expected %s, got %s", tt.expected, string(body))
			}

			// Stop the server.
			if err := r.Stop(); err != nil {
				t.Errorf("Failed to stop the server: %v", err)
			}

			// Wait for the server to shut down completely.
			<-done
			done = nil
		})
	}
}

// TestOverallWithComplexURLsConccurent tests the overall behavior of the router when handling requests with complex URLs and query parameters concurrently.
// It sets the desired concurrency level, defines multiple test cases with different URL patterns and query parameters, creates a router,
// compiles a regular expression from the endpoint and parameter pairs, registers a handler function for the compiled regular expression,
// starts the server, sends concurrent HTTP requests to the specified URLs and methods, verifies the response bodies against the expected values,
// stops the server, and waits for the server to shut down completely.
func TestOverallWithComplexURLsConccurent(t *testing.T) {
	// Set desired concurrency level.
	c := concurrentRequests

	// Define test cases.
	tests := []struct {
		testName string
		endpoint string
		pairs    []string
		url      string
		method   string
		expected string
	}{
		{
			testName: "Users path test with query parameters",
			endpoint: "/users",
			pairs:    []string{"id", `[1-9]\d*`, "category", `[0-9]\d*`},
			url:      "http://localhost:8000/users/1/3?name=John&age=30",
			method:   http.MethodGet,
			expected: "Path parameters are: id=1, category=3\nQuery parameters are: name=John, age=30",
		},
		{
			testName: "Inner subpath test with query parameters",
			endpoint: "/users",
			pairs:    []string{"id", IdPattern, "category", "category", "category_id", IdPattern},
			url:      "http://localhost:8000/users/1/category/55?name=John&age=30",
			method:   http.MethodGet,
			expected: "Path parameters are: id=1, category=category, category_id=55\nQuery parameters are: name=John, age=30",
		},
		{
			testName: "Pre-defined Regex Test with query parameters",
			endpoint: "/users",
			pairs:    []string{"id", IdPattern, "category", WordCaseInsensitivePattern},
			url:      "http://localhost:8000/users/1/ThisIsAPascalCasePattern?name=John&age=30",
			method:   http.MethodGet,
			expected: "Path parameters are: id=1, category=ThisIsAPascalCasePattern\nQuery parameters are: name=John, age=30",
		},
		{
			testName: "Products path test with query parameters",
			endpoint: "/products",
			pairs:    []string{"id", `[1-9]\d*`, "category", `[0-9]\d*`},
			url:      "http://localhost:8000/products/9/5?name=John&age=30",
			method:   http.MethodGet,
			expected: "Path parameters are: id=9, category=5\nQuery parameters are: name=John, age=30",
		},
	}

	// Iterate test cases.
	for _, tt := range tests {
		t.Run(tt.testName, func(t *testing.T) {
			// Instanciate the router.
			r := NewRouter(semCount, nil, nil)

			// Define the regex.
			regex, err := CompileRegex(tt.endpoint, tt.pairs...)
			if err != nil {
				t.Errorf("CompileRegex failed: %v", err)
			}

			// Define the handler.
			handler := func(w http.ResponseWriter, r *http.Request) {
				// Define all variables.
				allVariables := Vars(r.Context())

				// Split the raw query string by '&' to get individual query parameters
				queryParams := strings.Split(r.URL.RawQuery, "&")

				// Define an empty string variable to hold the path parameters.
				pathParamsStr := ""

				// Iterate keys and values (pairs).
				for i, param := range tt.pairs {
					if i%2 == 0 {
						// Define the key.
						key := param

						// Define the value.
						value := allVariables[key]

						// Append to the string.
						pathParamsStr += fmt.Sprintf("%s=%s, ", key, value)
					}
				}

				// Remove the last comma and space.
				pathParamsStr = strings.TrimSuffix(pathParamsStr, ", ")

				// Define an empty string variable to hold the query parameters.
				queryParamsStr := ""

				// Iterate keys and values.
				for _, param := range queryParams {

					// Split each query parameter by '=' to get the key and value.
					keyValue := strings.SplitN(param, "=", 2)

					if len(keyValue) == 2 {
						// Define the key.
						key := keyValue[0]

						// Define the value.
						value := keyValue[1]

						// Build the query parameters string.
						queryParamsStr += key + "=" + value + ", "
					}
				}

				// Remove the last comma and space.
				queryParamsStr = strings.TrimSuffix(queryParamsStr, ", ")

				// Define the expected string.
				expected := fmt.Sprintf("Path parameters are: %s\nQuery parameters are: %s", pathParamsStr, queryParamsStr)

				// Write the response.
				fmt.Fprint(w, expected)
			}

			// Register the route.
			r.Register(regex, handler, Unlimited, tt.method)

			// Create a channel to signal when the server has been stopped.
			done := make(chan bool)

			// Start the server in the background.
			go func() {
				// Run.
				err := r.Run("localhost:8000", 120*time.Second, 5*time.Second, 5*time.Second)
				if err != nil && err != http.ErrServerClosed {
					t.Errorf("Failed to start the server on localhost:8000: %v", err)
				} else {
					done <- true
				}
			}()

			// Wait for the server to start.
			if err := testjuice.WaitForServer(localhost8000URL, waitForServerDelay); err != nil {
				t.Errorf("failed to wait for the server to start: %v", err)
			}

			// Reset the server concurrency times for each test.
			var calledCounter int64 = 0
			var endCounter int64 = 0
			var wgLoop sync.WaitGroup

			// Launch all concurrent requests
			for i := 0; i < c; i++ {
				wgLoop.Add(1)

				// Make a request to the server.
				log.Printf("✅ Go func #%s started", fmt.Sprint(i))

				// Launch Go routine.
				go func() {
					defer func() {
						atomic.AddInt64(&endCounter, 1)

						wgLoop.Done()
					}()

					atomic.AddInt64(&calledCounter, 1)

					// Perform the request.
					resp, err := http.Get(tt.url)
					if err != nil {
						log.Printf("💥 HTTP request failed")
						t.Errorf("failed to make request: %v", err)
					}

					// Defer body closure.
					defer resp.Body.Close()

					// Read the response body.
					body, err := io.ReadAll(resp.Body)
					if err != nil {
						t.Errorf("Failed to read response body: %v", err)
					}

					// Compare the response to the expected value.
					if string(body) != tt.expected {
						t.Errorf("Unexpected response, expected %s, got %s", tt.expected, string(body))
					}
				}()
			}

			// Wait for all goroutines to complete.
			wgLoop.Wait()

			log.Printf("💡 %d Go funcs started", calledCounter)
			log.Printf("💡 %d Go funcs ended", endCounter)
			log.Printf("💡 %d Go funcs failed", calledCounter-endCounter)

			// Reset counters.
			calledCounter = 0
			endCounter = 0

			// Stop the server.
			if err := r.Stop(); err != nil {
				t.Errorf("failed to stop the server: %v", err)
			}

			// Wait for the server to shut down completely.
			<-done
			done = nil
		})
	}
}
