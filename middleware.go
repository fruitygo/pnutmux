package pnutmux

import (
	"compress/gzip"
	"context"
	"log/slog"
	"net/http"
	"os"
	"strings"

	"gitlab.com/fruitygo/gojuice/juice/logjuice"
	"gitlab.com/fruitygo/pnutmux/contract"
	"gitlab.com/fruitygo/pnutmux/internal/adapter/util/logger"
	"gitlab.com/fruitygo/pnutmux/pnutconfig"
)

// MiddlewareFunc represents a middleware function. It takes an http.HandlerFunc as input and returns an http.HandlerFunc.
//
// This type can be used to define middleware functions that can be chained together using the Chain function.
type MiddlewareFunc func(http.HandlerFunc) http.HandlerFunc

// Chain combines multiple MiddlewareFunc functions into a single MiddlewareFunc, creating a middleware chain.
// The returned MiddlewareFunc applies each input middleware function to the next one in reverse order.
//
// The function takes a variadic number of MiddlewareFunc functions as input. The order of the input functions
// represents the order in which they will be applied in the middleware chain, with the first function being applied last.
//
// The returned MiddlewareFunc can be used as a middleware in an HTTP server because it follows the http.HandlerFunc interface.
func Chain(handlers ...MiddlewareFunc) MiddlewareFunc {
	return func(h http.HandlerFunc) http.HandlerFunc {
		// Wrap the final handler with all the middleware functions in reverse order.
		for i := len(handlers) - 1; i >= 0; i-- {
			h = handlers[i](h)
		}

		return h
	}
}

// cors returns an http.Handler that adds Cross-Origin Resource Sharing (CORS) headers to the HTTP response.
//
// The returned handler calls the next handler after setting the CORS headers.
//
// The function checks the value of the "ALLOWED_ORIGINS" environment variable and sets the "Access-Control-Allow-Origin"
// header accordingly. If the "ALLOWED_ORIGINS" environment variable is not set, is empty, or contains "*", the function
// sets the "Access-Control-Allow-Origin" header to "*", allowing requests from any origin.
//
// The function also sets the "Access-Control-Allow-Headers" header to "*", allowing any headers in the actual request.
//
// The returned handler can be used as a middleware in an HTTP server because it follows the http.Handler interface.
func cors(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		// Look up the value of the "ALLOWED_ORIGINS" environment variable.
		allowedOrigins, exists := os.LookupEnv(pnutconfig.AllowedOriginsKey)

		if !exists || allowedOrigins == "" || allowedOrigins == "*" {
			w.Header().Set(pnutconfig.AccessControlAllowOriginKey, "*")
		} else {
			for _, allowedOrigin := range strings.Split(allowedOrigins, ",") {
				if r.Header.Get("Origin") == allowedOrigin {
					w.Header().Set(pnutconfig.AccessControlAllowOriginKey, allowedOrigin)

					break
				}
			}
		}

		// Set the "Access-Control-Allow-Headers" and "Access-Control-Allow-Methods" headers.
		w.Header().Set(pnutconfig.AccessControlAllowHeadersKey, "*")
		w.Header().Set(pnutconfig.AccessControlAllowMethodsKey, "*") // To be overriden by ServeHTTP method.

		// Handle preflight request.
		if r.Method == http.MethodOptions {
			w.WriteHeader(http.StatusOK)
			return
		}

		next.ServeHTTP(w, r)
	})
}

// -----------------------------
// Gzip middleware
// -----------------------------

// Gzip returns an http.HandlerFunc that compresses the HTTP response using gzip compression.
//
// It checks the "Accept-Encoding" header of the HTTP request to determine if the client supports gzip compression.
func Gzip(next http.HandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		if !strings.Contains(r.Header.Get("Accept-Encoding"), "gzip") {
			next.ServeHTTP(w, r)
			return
		}

		// Create a new logger that writes to the standard output.
		logger := logger.NewAdapter(os.Stdout, slog.LevelInfo, nil)

		// Define a new wrapped response writer that compresses the response using gzip compression.
		wrw := NewWrappedResponseWriter(logger, w)

		// Delete the "Content-Length" header and set the "Content-Encoding" header to "gzip" to trigger chunked transfer encoding.
		wrw.Header().Del("Content-Length")
		wrw.Header().Set("Content-Encoding", "gzip")

		defer func() {
			wrw.Flush()
		}()

		// Call the next handler in the chain.
		next.ServeHTTP(wrw, r)

		// Log a message indicating that gzip compression was successful.
		logger.SLog(r.Context(), logjuice.Info, "🗜️", "Gzip compression successful")
	}
}

// wrappedResponseWriter is a custom response writer that wraps an http.ResponseWriter and compresses the response using gzip compression.
type wrappedResponseWriter struct {
	l  contract.Logger
	rw http.ResponseWriter
	gw *gzip.Writer
}

// NewWrappedResponseWriter creates a new instance of the wrappedResponseWriter struct.
func NewWrappedResponseWriter(l contract.Logger, rw http.ResponseWriter) *wrappedResponseWriter {
	return &wrappedResponseWriter{
		l:  l,
		rw: rw,
		gw: gzip.NewWriter(rw),
	}
}

// -----------------------------
// Implement http.ResponseWriter
// -----------------------------

// Header returns the header map that will be sent by the wrappedResponseWriter.
func (ww *wrappedResponseWriter) Header() http.Header {
	return ww.rw.Header()
}

// Write writes the data to the response writer.
func (ww *wrappedResponseWriter) Write(b []byte) (int, error) {
	// If the gzip writer is nil, write the data to the response writer.
	if ww.gw == nil {
		return ww.rw.Write(b)
	}

	return ww.gw.Write(b)
}

// WriteHeader sends an HTTP response header with the provided status code.
func (ww *wrappedResponseWriter) WriteHeader(statusCode int) {
	ww.rw.WriteHeader(statusCode)
}

// -----------------------------
// Implement http.Flusher
// -----------------------------

// Flush sends any buffered data to the client.
func (ww *wrappedResponseWriter) Flush() {
	if ww.gw != nil {
		if err := ww.gw.Flush(); err != nil {
			ww.l.SLog(context.Background(), logjuice.Error, "", err.Error())
		}

		if err := ww.gw.Close(); err != nil {
			ww.l.SLog(context.Background(), logjuice.Error, "", err.Error())
		}
	}
}
