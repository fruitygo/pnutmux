package pnutmux

import (
	"testing"

	"gitlab.com/fruitygo/pnutmux/pnutconfig"
)

// TestSubpath tests the Subpath function.
func TestSubpath(t *testing.T) {
	expected := pnutconfig.SubpathKey

	// Call the Subpath function to get the result.
	result := Subpath()

	// Check if the result matches the expected value.
	if result != expected {
		t.Errorf("expected subpath to be %q, but got %q", expected, result)
	}
}
