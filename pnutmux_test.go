package pnutmux

import (
	"bytes"
	"context"
	"fmt"
	"io"
	"net/http"
	"net/http/httptest"
	"reflect"
	"regexp"
	"testing"
)

// Define the maximum concurrency and maximum work pool size.
var semCount uint = 10

// TestExtractParams validates the behavior of the extractParams function by testing various input scenarios and comparing the output with the expected results.
// It focuses on testing the function's ability to correctly extract URL parameters based on a given pattern and verifies the matching and non-matching patterns.
func TestExtractParams(t *testing.T) {
	// Define test cases.
	testCases := []struct {
		expected      map[string]string
		name          string
		requestURL    string
		pattern       string
		expectedMatch bool
	}{
		{
			name:          "Matching pattern with multiple subpaths to be ignored",
			requestURL:    "/users/123/category/123/product/125",
			pattern:       "^/users/(?P<id>[0-9]+)/(?P<subpath>category)/(?P<category_id>[0-9]+)/(?P<subpath>product)/(?P<product_id>[0-9]+)$",
			expectedMatch: true,
			expected:      map[string]string{"endpoint": "users", "id": "123", "category_id": "123", "product_id": "125"},
		},
		{
			name:          "Empty Regex",
			requestURL:    "/users",
			pattern:       "",
			expectedMatch: false,
			expected:      map[string]string{},
		},
		{
			name:          "Matching pattern with one parameter",
			requestURL:    "/users/123",
			pattern:       "^/users/(?P<id>[0-9]+)$",
			expectedMatch: true,
			expected:      map[string]string{"endpoint": "users", "id": "123"},
		},
		{
			name:          "Non-matching pattern",
			requestURL:    "/users/abc",
			pattern:       "^/users/(?P<id>[0-9]+)$",
			expectedMatch: false,
			expected:      map[string]string{},
		},
	}

	// Iterate test cases.
	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			// Create a new HTTP request with the given request URL.
			request, _ := http.NewRequest("GET", tc.requestURL, nil)

			// Define a map of path parameters.
			var params map[string]string

			// Invoke the extractParams function with the request and the compiled pattern to extract the parameters.
			if tc.pattern == "" {
				params = extractParams(request, nil)
			} else {
				params = extractParams(request, regexp.MustCompile(tc.pattern))
			}

			// Validate the extracted parameters based on the expected match result and the expected map of parameters.
			if !tc.expectedMatch && len(params) != 0 {
				t.Errorf("expected no params, got %d", len(params))
			} else if tc.expectedMatch && len(params) != len(tc.expected) {
				t.Errorf("expected %d params, got %d", len(tc.expected), len(params))
			}

			// Compare each expected parameter value with the actual parameter value and report any mismatches.
			for k, v := range tc.expected {
				if params[k] != v {
					t.Errorf("expected %s=%s, got %s=%s", k, v, k, params[k])
				}
			}
		})
	}
}

// TestVars validates the behavior of the Vars function by testing different input scenarios and comparing the output with the expected results.
// It focuses on testing the function's ability to extract variables from a given context and verifies the handling of various context states and values.
func TestVars(t *testing.T) {
	var testVarsKey requestParamsKey

	// Define test cases.
	tests := []struct {
		expected map[string]string
		ctx      context.Context
		name     string
	}{
		{
			name: "valid context with parameters",
			ctx: context.WithValue(context.Background(), testVarsKey, &requestParams{
				params: map[string]string{
					"param1": "value1",
					"param2": "value2",
				},
			}),
			expected: map[string]string{
				"param1": "value1",
				"param2": "value2",
			},
		},
		{
			name:     "valid empty context",
			ctx:      context.Background(),
			expected: nil,
		},
		{
			name:     "invalid context",
			ctx:      context.WithValue(context.Background(), testVarsKey, "invalid"),
			expected: nil,
		},
	}

	// Iterate test cases.
	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			// Invoke the Vars function with the given context to extract the variables.
			result := Vars(test.ctx)

			// Compare the extracted variables with the expected map of variables and report any mismatches.
			if !reflect.DeepEqual(result, test.expected) {
				t.Errorf("Vars() failed: expected %v, but got %v", test.expected, result)
			}
		})
	}
}

// TestVar validates the behavior of the Var function by testing different input scenarios and comparing the output with the expected results.
// It focuses on testing the function's ability to extract a variable value from a given context and verifies the handling of various context states and keys.
func TestVar(t *testing.T) {
	var testVarKey requestParamsKey

	// Define test cases.
	tests := []struct {
		name      string
		ctx       context.Context
		key       string
		wantValue string
	}{
		{
			name: "Key exists",
			ctx: context.WithValue(context.Background(), testVarKey, &requestParams{
				params: map[string]string{
					"foo": "bar",
				},
			}),
			key:       "foo",
			wantValue: "bar",
		},
		{
			name:      "Key does not exist",
			ctx:       context.Background(),
			key:       "foo",
			wantValue: "",
		},
		{
			name:      "Context does not contain requestParams",
			ctx:       context.Background(),
			key:       "foo",
			wantValue: "",
		},
	}

	// Iterate test cases.
	for _, tc := range tests {
		t.Run(tc.name, func(t *testing.T) {
			// Invoke the Var function with the given context and key to extract the variable value.
			if gotValue := Var(tc.ctx, tc.key); gotValue != tc.wantValue {
				t.Errorf("Var() returned %q; want %q", gotValue, tc.wantValue)
			}
		})
	}
}

// TestExtractQueryParams validates the behavior of the extractQueryParams function by testing different input scenarios and comparing the output with the expected results.
// It focuses on testing the function's ability to extract query parameters from a given request URL and verifies the handling of various query parameter combinations.
func TestExtractQueryParams(t *testing.T) {
	// Define test cases.
	testCases := []struct {
		expectedParams map[string][]string
		name           string
		requestURL     string
	}{
		{
			name:           "Empty query parameters",
			requestURL:     "/users",
			expectedParams: map[string][]string{},
		},
		{
			name:           "Single query parameter",
			requestURL:     "/users?param1=value1",
			expectedParams: map[string][]string{"param1": {"value1"}},
		},
		{
			name:           "Single query parameter with %20 encoding",
			requestURL:     "/users?param1=value1%20value2",
			expectedParams: map[string][]string{"param1": {"value1 value2"}},
		},
		{
			name:           "Multiple query parameters",
			requestURL:     "/users?param1=value1&param2=value2",
			expectedParams: map[string][]string{"param1": {"value1"}, "param2": {"value2"}},
		},
		{
			name:           "Duplicate query parameters",
			requestURL:     "/users?param1=value1&param1=value2",
			expectedParams: map[string][]string{"param1": {"value1", "value2"}},
		},
		{
			name:           "Query parameters with subpath in path",
			requestURL:     "/users/123/subpath?param1=value1&param2=value2",
			expectedParams: map[string][]string{"param1": {"value1"}, "param2": {"value2"}},
		},
	}

	// Iterate test cases.
	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			// Create a new HTTP request with the given request URL.
			request, _ := http.NewRequest("GET", tc.requestURL, nil)

			// Invoke the extractQueryParams function to extract the query parameters from the request.
			params := extractQueryParams(request)

			// Compare the extracted query parameters with the expected map of query parameters and report any mismatches.
			if len(params) != len(tc.expectedParams) {
				t.Errorf("expected %d params, got %d", len(tc.expectedParams), len(params))
			}

			// Verify that each query parameter has the expected value.
			for k, v := range tc.expectedParams {
				if !reflect.DeepEqual(params[k], v) {
					t.Errorf("expected %s=%s, got %s=%s", k, v, k, params[k])
				}
			}
		})
	}
}

// TestQueryVar is a test function that verifies the behavior of the QueryVar function.
// It defines multiple test cases to cover different scenarios.
// For each test case, it sets up the necessary context and key-value pairs, and then calls the QueryVar function.
// The function compares the returned value with the expected value and reports any discrepancies as test failures.
// This test function ensures that the QueryVar function correctly retrieves the value associated with a given key from the context.
func TestQueryVar(t *testing.T) {
	var testQueryVarKey requestParamsKey
	// Define test cases.
	tests := []struct {
		name      string
		ctx       context.Context
		key       string
		wantValue []string
	}{
		{
			name: "Key exists",
			ctx: context.WithValue(context.Background(), testQueryVarKey, &requestParams{
				queryParams: map[string][]string{
					"foo": {"bar"},
				},
			}),
			key:       "foo",
			wantValue: []string{"bar"},
		},
		{
			name:      "Key does not exist",
			ctx:       context.Background(),
			key:       "foo",
			wantValue: []string{},
		},
		{
			name:      "Context does not contain requestParams",
			ctx:       context.Background(),
			key:       "foo",
			wantValue: []string{},
		},
	}

	// Iterate test cases.
	for _, tc := range tests {
		t.Run(tc.name, func(t *testing.T) {
			gotValue := QueryVar(tc.ctx, tc.key)

			if !reflect.DeepEqual(gotValue, tc.wantValue) {
				t.Errorf("QueryVar() returned %q; want %q", gotValue, tc.wantValue)
			}
		})
	}
}

// TestQueryVars validates the behavior of the QueryVars function by testing different input scenarios and comparing the output with the expected results.
// It focuses on testing the function's ability to extract query parameters from the context and verify the correctness of the extracted parameters.
func TestQueryVars(t *testing.T) {
	var testQueryVarsKey requestParamsKey

	// Define test cases.
	tests := []struct {
		name     string
		ctx      context.Context
		expected map[string][]string
	}{
		{
			name: "Valid context with query parameters",
			ctx: context.WithValue(context.Background(), testQueryVarsKey, &requestParams{
				queryParams: map[string][]string{
					"param1": {"value1"},
					"param2": {"value2"},
				},
			}),
			expected: map[string][]string{
				"param1": {"value1"},
				"param2": {"value2"},
			},
		},
		{
			name:     "Valid empty context",
			ctx:      context.Background(),
			expected: nil,
		},
		{
			name:     "Invalid context",
			ctx:      context.WithValue(context.Background(), testQueryVarsKey, "invalid"),
			expected: nil,
		},
	}

	// Iterate test cases.
	for _, tc := range tests {
		t.Run(tc.name, func(t *testing.T) {
			// Call the QueryVars function with the given context and obtain the result.
			result := QueryVars(tc.ctx)

			// Compare the extracted query parameters with the expected map of query parameters and report any mismatches.
			if !reflect.DeepEqual(result, tc.expected) {
				t.Errorf("QueryVars() failed: expected %v, but got %v", tc.expected, result)
			}
		})
	}
}

// TestValidHTTPMethod validates the behavior of the validHTTPMethod function by testing different HTTP methods.
// It verifies that the function returns the expected result for each method, both valid and invalid.
func TestValidHTTPMethod(t *testing.T) {
	// Define test cases.
	testCases := []struct {
		method   string
		expected bool
	}{
		{
			method:   http.MethodGet,
			expected: true,
		},
		{
			method:   http.MethodPost,
			expected: true,
		},
		{
			method:   http.MethodPut,
			expected: true,
		},
		{
			method:   http.MethodDelete,
			expected: true,
		},
		{
			method:   http.MethodDelete,
			expected: true,
		},
		{
			method:   http.MethodTrace,
			expected: true,
		},
		{
			method:   http.MethodOptions,
			expected: true,
		},
		{
			method:   http.MethodPatch,
			expected: true,
		},
		{
			method:   http.MethodConnect,
			expected: true,
		},
		{
			method:   "INVALID",
			expected: false,
		},
	}

	// Iterate test cases.
	for _, tc := range testCases {
		t.Run(tc.method, func(t *testing.T) {
			// Invoke the validHTTPMethod function with the test case method.
			result := validHTTPMethod(tc.method)

			// Compare the result with the expected value.
			if result != tc.expected {
				t.Errorf("expected %v for method %s, but got %v", tc.expected, tc.method, result)
			}
		})
	}
}

// TestCompileRegex validates the behavior of the Compregx function by testing different endpoint and pattern pairs.
// It verifies that the function returns the expected regular expression pattern for the given endpoint and pairs.
func TestCompileRegex(t *testing.T) {
	// Define test cases.
	tests := []struct {
		endpoint string
		pairs    []string
		expected *regexp.Regexp
		wantErr  bool
	}{
		{
			endpoint: "/",
			pairs:    []string{},
			expected: regexp.MustCompile(`^/$`),
			wantErr:  false,
		},
		{
			endpoint: "/users",
			pairs:    []string{"id", `\d+`, "name", `[a-zA-Z]+`},
			expected: regexp.MustCompile(`^/users/(?P<id>\d+)/(?P<name>[a-zA-Z]+)$`),
			wantErr:  false,
		},
		{
			endpoint: "/users",
			pairs:    []string{"id", `\d+`, "name", `[a-zA-Z]+`, "age", `\d+`},
			expected: regexp.MustCompile(`^/users/(?P<id>\d+)/(?P<name>[a-zA-Z]+)/(?P<age>\d+)$`),
			wantErr:  false,
		},
		{
			endpoint: "&(*^&)", // invalid characters in the endpoint
			pairs:    []string{},
			expected: nil,
			wantErr:  true,
		},
		{
			endpoint: "", // empty endpoint
			pairs:    []string{"id", `\d+`, "name", `[a-zA-Z]+`, "age", `\d+`},
			expected: nil,
			wantErr:  true,
		},
		{
			endpoint: "/users",
			pairs:    []string{"id", `\d+`, "name", `[a-zA-Z]+`, "age"}, // invalid number of parameters
			expected: nil,
			wantErr:  true,
		},
		{
			endpoint: "/users",
			pairs:    []string{"id", `\d+`, "id", `[a-zA-Z]+`}, // duplicate key
			expected: nil,
			wantErr:  true,
		},
		{
			endpoint: "/users",
			pairs:    []string{"id", `\d+`, "name[[", `[a-zA-Z]+`, "age", `\d+`},
			expected: regexp.MustCompile(`^/users/(?P<id>\d+)/(?P<name>[a-zA-Z]+)/(?P<age>\d+)$`),
			wantErr:  true,
		},
		{
			endpoint: "/users", // QuoteMeta returning an error
			pairs:    []string{"id", `\d+`, "((name[[", `[a-zA-Z]+`, "age", `\d+`},
			expected: nil,
			wantErr:  true,
		},
	}

	// Loop test cases.
	for _, tt := range tests {
		t.Run(fmt.Sprintf("Compregx_%v", tt.endpoint), func(t *testing.T) {
			// Call the function.
			result, err := CompileRegex(tt.endpoint, tt.pairs...)

			if tt.wantErr {
				// Verify error.
				if err == nil {
					t.Errorf("CompileRegex(%v, %v) expected an error but got nil", tt.endpoint, tt.pairs)
				}
			} else {
				if err != nil {
					t.Errorf("CompileRegex(%v, %v) failed with error %v", tt.endpoint, tt.pairs, err)
				} else if result == nil {
					t.Errorf("CompileRegex(%v, %v) returned nil without an error", tt.endpoint, tt.pairs)
				} else if result.String() != tt.expected.String() {
					t.Errorf("CompileRegex(%v, %v) = %v, want %v", tt.endpoint, tt.pairs, result, tt.expected)
				}
			}
		})
	}
}

// TestSetAllowedOrigin validates the behavior of the SetAllowedOrigin function by testing different origin values.
// It verifies that the function sets the ALLOWED_ORIGINS environment variable using the provided origin value.
// The test cases cover different scenarios such as successful environment variable setting and error cases.
// The expected field in each test case is used to define the expected error value returned by the function.
// The test ensures that the SetAllowedOrigin function behaves as intended for various input scenarios.
func TestSetAllowedOrigin(t *testing.T) {
	// Define test cases.
	tests := []struct {
		origin    string
		wantError bool
	}{
		{
			origin:    "https://example.com",
			wantError: false,
		},
		{
			origin:    "https://api.example.com,http://test.example.com",
			wantError: false,
		},
		{
			origin:    "",
			wantError: true,
		},
	}

	// Iterate test cases.
	for _, tt := range tests {
		t.Run(fmt.Sprintf("SetAllowedOrigin(%q)", tt.origin), func(t *testing.T) {
			var err error

			if tt.wantError {
				err = setAllowedOriginWithName(tt.origin, "")
			} else {
				err = SetAllowedOrigin(tt.origin)
			}

			if tt.wantError && err == nil {
				t.Errorf("SetAllowedOrigin(%q) expected an error, but got nil", tt.origin)
			} else if !tt.wantError && err != nil {
				t.Errorf("SetAllowedOrigin(%q) unexpected error: %v", tt.origin, err)
			}
		})
	}
}

// TestRespond tests the Respond method of the PnutRouter by setting up a test HTTP server,
// making a request, and validating the response status code.
func TestRespond(t *testing.T) {
	// Define test cases.
	tests := []struct {
		data           interface{}
		name           string
		expectedStatus int
	}{
		{
			name:           "Normal data",
			data:           map[string]interface{}{"message": "Hello, world!"},
			expectedStatus: http.StatusOK,
		},
		{
			name:           "Unserializable data",
			data:           func() {},
			expectedStatus: http.StatusInternalServerError,
		},
	}

	// Iterate test cases.
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			// Define a new server.
			server := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
				Respond(w, tt.data, http.StatusOK, false)
			}))

			// Defer server closure.
			defer server.Close()

			// Perform request.
			resp, err := http.Get(server.URL)
			if err != nil {
				t.Fatalf("error making request: %v", err)
			}

			// Defer body closure.
			defer resp.Body.Close()

			// Verify status code.
			if resp.StatusCode != tt.expectedStatus {
				t.Errorf("expected status code %d, got %d", tt.expectedStatus, resp.StatusCode)
			}
		})
	}
}

// TestRespondErr tests the RespondError method of the PnutRouter by setting up a test HTTP server,
// making a request, and validating the response status code.
func TestRespondErr(t *testing.T) {
	// Define a server.
	server := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		RespondErr(w, http.StatusInternalServerError)
	}))

	// Defer server closure.
	defer server.Close()

	// Perform request.
	resp, err := http.Get(server.URL)
	if err != nil {
		t.Fatalf("error making request: %v", err)
	}

	// Defer body closure.
	defer resp.Body.Close()

	// Verify status code.
	if resp.StatusCode != http.StatusInternalServerError {
		t.Errorf("expected status code %d, got %d", http.StatusInternalServerError, resp.StatusCode)
	}
}

// mockReader returns an io.Reader with the given HTML content.
func mockReader(content string) io.Reader {
	return bytes.NewBufferString(content)
}

// TestServeHTML tests the ServeHTML method of the PnutRouter by setting up a test HTTP server,
// making a request, and validating the response status code.
func TestServeHTML(t *testing.T) {
	// Define test cases.
	tests := []struct {
		name           string
		htmlContent    io.Reader
		expectedStatus int
		expectedError  bool
	}{
		{
			name:           "Successful response",
			htmlContent:    mockReader("<html><body><h1>Hello, World!</h1></body></html>"),
			expectedStatus: http.StatusOK,
			expectedError:  false,
		},
		{
			name:           "Nil HTML content",
			htmlContent:    nil,
			expectedStatus: http.StatusInternalServerError,
			expectedError:  true,
		},
	}

	// Iterate test cases.
	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			// Define a server.
			server := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
				ServeHTML(w, test.htmlContent, http.StatusOK)
			}))

			// Defer server closure.
			defer server.Close()

			// Perform request.
			resp, err := http.Get(server.URL)
			if err != nil {
				t.Fatalf("error making request: %v", err)
			}

			// Defer body closure.
			defer resp.Body.Close()

			// Verify status code.
			if resp.StatusCode != test.expectedStatus {
				t.Errorf("expected status code %d, got %d", test.expectedStatus, resp.StatusCode)
			}

			// Verify error.
			if test.expectedError && resp.StatusCode != http.StatusInternalServerError {
				t.Error("expected an error, but no error occurred")
			}

			// Verify error.
			if !test.expectedError && resp.StatusCode == http.StatusInternalServerError {
				t.Error("did not expect an error, but an error occurred")
			}
		})
	}
}
