// This test uses a constant number of 10 calls per day to set the daily limit.
// It uses 3 constants, zrangeResultsSumEight, zrangeResultsSumTen and zrangeResultSumTwelve,
// to simulate the result of the ZRange command.
// The first 2 constants simulate the result of the ZRange command when the daily limit has not been reached.
// The last constant simulates the result of the ZRange command when the daily limit has been reached.
package limiter

import (
	"log/slog"
	"net/http"
	"net/http/httptest"
	"os"
	"regexp"
	"sync"
	"testing"
	"time"

	"github.com/redis/go-redis/v9"
	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/require"
	"github.com/stretchr/testify/suite"
	"gitlab.com/fruitygo/pnutmux/internal/adapter/util/logger"
	"gitlab.com/fruitygo/pnutmux/internal/adapter/util/responder"
	"gitlab.com/fruitygo/pnutmux/mocks"
	"gitlab.com/fruitygo/pnutmux/pnutconfig"
)

var (
	zrangeResultsSumEight    []string = []string{"12345.00:4", "12345.00:4"} // Sum of weights is 8
	zrangeResultsSumTen      []string = []string{"12345.00:5", "12345.00:5"} // Sum of weights is 10
	zrangeResultSumTwelve    []string = []string{"12345.00:6", "12345.00:6"} // Sum of weights is 12
	zrangeResultMalformatted []string = []string{"12345.00:x"}               // Malformatted result with a letter
	maxDailyScore            uint     = 10                                   // Number of calls per day is 10
	groupID                  string   = "microservice"                       // Group ID
	headerKey                string   = "X-Header-Key"
	emptyHeaderKey           string   = ""
)

func TestEngageSuite(t *testing.T) {
	suite.Run(t, new(limiterTestSuite))
}

type limiterTestSuite struct {
	suite.Suite
	logger          *logger.Adapter
	responder       *responder.Adapter
	mockRedisClient *mocks.RedisClient
	mockPipeliner   *mocks.Pipeliner
	adapter         *Adapter
}

func (suite *limiterTestSuite) SetupTest() {
	suite.logger = logger.NewAdapter(os.Stdout, slog.LevelDebug, nil)
	suite.responder = responder.NewAdapter()
}

func (suite *limiterTestSuite) TestEngage() {
	testCases := []struct {
		name                 string
		authHeaderKey        string
		authHeader           string
		urlPath              string
		method               string
		timeout              time.Duration
		routes               map[*regexp.Regexp]*pnutconfig.Route
		expectedCode         int
		returnArgsExec       []interface{}
		adapterAuthHeaderKey *string
	}{
		// Test case 1: Successful request.
		{
			name:          "SuccessfulRequest",
			authHeaderKey: headerKey,
			authHeader:    "header_value",
			urlPath:       "/valid",
			method:        "GET",
			timeout:       500 * time.Millisecond,
			routes:        map[*regexp.Regexp]*pnutconfig.Route{regexp.MustCompile("/valid"): {Weights: map[string]pnutconfig.RequestWeight{"GET": 1}}},
			expectedCode:  http.StatusOK,
			returnArgsExec: []interface{}{
				redis.NewIntResult(4, nil),                           // Mimics result from ZRemRangeByScore
				redis.NewIntResult(4, nil),                           // Mimics result from ZAdd
				redis.NewStringSliceResult(zrangeResultsSumTen, nil), // Mimics result from ZRange with 2 documents found
				redis.NewBoolResult(true, nil),                       // Mimics result from Expire
			},
			adapterAuthHeaderKey: &headerKey,
		},
		// Test case 2: Empty auth header to be replaced by remote address.
		{
			name:          "EmptyAuthHeader",
			authHeaderKey: headerKey,
			authHeader:    "",
			urlPath:       "/protected",
			method:        "POST",
			timeout:       500 * time.Millisecond,
			routes:        map[*regexp.Regexp]*pnutconfig.Route{regexp.MustCompile("/protected"): {Weights: map[string]pnutconfig.RequestWeight{"POST": 1}}},
			expectedCode:  http.StatusOK,
			returnArgsExec: []interface{}{
				redis.NewIntResult(4, nil),                             // Mimics result from ZRemRangeByScore
				redis.NewIntResult(4, nil),                             // Mimics result from ZAdd
				redis.NewStringSliceResult(zrangeResultsSumEight, nil), // Mimics result from ZRange with 2 documents found
				redis.NewBoolResult(true, nil),                         // Mimics result from Expire
			},
			adapterAuthHeaderKey: &headerKey,
		},
		// Test case 3: Regex not matching url path.
		{
			name:          "RegexNotMatching",
			authHeaderKey: headerKey,
			authHeader:    "header_value",
			urlPath:       "/protected",
			method:        "POST",
			timeout:       500 * time.Millisecond,
			routes:        map[*regexp.Regexp]*pnutconfig.Route{regexp.MustCompile("/valid"): {Weights: map[string]pnutconfig.RequestWeight{"POST": 1}}},
			expectedCode:  http.StatusInternalServerError,
			returnArgsExec: []interface{}{
				redis.NewIntResult(4, nil),                             // Mimics result from ZRemRangeByScore
				redis.NewIntResult(4, nil),                             // Mimics result from ZAdd
				redis.NewStringSliceResult(zrangeResultsSumEight, nil), // Mimics result from ZRange with 2 documents found
				redis.NewBoolResult(true, nil),                         // Mimics result from Expire
			},
			adapterAuthHeaderKey: &headerKey,
		},
		// Test case 4: Request exceeding daily limit.
		{
			name:          "ExceedDailyLimit",
			authHeaderKey: headerKey,
			authHeader:    "header_value",
			urlPath:       "/exceed",
			method:        "GET",
			timeout:       500 * time.Millisecond,
			routes:        map[*regexp.Regexp]*pnutconfig.Route{regexp.MustCompile("/exceed"): {Weights: map[string]pnutconfig.RequestWeight{"GET": 1}}},
			expectedCode:  http.StatusTooManyRequests,
			returnArgsExec: []interface{}{
				redis.NewIntResult(4, nil),                             // Mimics result from ZRemRangeByScore
				redis.NewIntResult(4, nil),                             // Mimics result from ZAdd
				redis.NewStringSliceResult(zrangeResultSumTwelve, nil), // Mimics result from ZRange with 2 documents found
				redis.NewBoolResult(true, nil),                         // Mimics result from Expire
			},
			adapterAuthHeaderKey: &headerKey,
		},
		// Test case 5: Timeout exceeded.
		{
			name:          "TimeoutExceeded",
			authHeaderKey: headerKey,
			authHeader:    "header_value",
			urlPath:       "/timeout",
			method:        "GET",
			timeout:       0 * time.Millisecond,
			routes:        map[*regexp.Regexp]*pnutconfig.Route{regexp.MustCompile("/timeout"): {Weights: map[string]pnutconfig.RequestWeight{"GET": 3}}},
			expectedCode:  http.StatusRequestTimeout,
			returnArgsExec: []interface{}{
				redis.NewIntResult(4, nil),                             // Mimics result from ZRemRangeByScore
				redis.NewIntResult(4, nil),                             // Mimics result from ZAdd
				redis.NewStringSliceResult(zrangeResultsSumEight, nil), // Mimics result from ZRange with 2 documents found
				redis.NewBoolResult(true, nil),                         // Mimics result from Expire
			},
			adapterAuthHeaderKey: &headerKey,
		},
		// Test case 6: No weight found for method.
		{
			name:                 "NoWeightForMethod",
			authHeaderKey:        headerKey,
			authHeader:           "header_value",
			urlPath:              "/valid",
			method:               "PUT",
			timeout:              500 * time.Millisecond,
			routes:               map[*regexp.Regexp]*pnutconfig.Route{regexp.MustCompile("/valid"): {Weights: map[string]pnutconfig.RequestWeight{"POST": 1}}},
			expectedCode:         http.StatusInternalServerError,
			returnArgsExec:       []interface{}{},
			adapterAuthHeaderKey: &headerKey,
		},
		// Test case 8: Successful request.
		{
			name:          "ParseIntError",
			authHeaderKey: headerKey,
			authHeader:    "header_value",
			urlPath:       "/valid",
			method:        "GET",
			timeout:       500 * time.Millisecond,
			routes:        map[*regexp.Regexp]*pnutconfig.Route{regexp.MustCompile("/valid"): {Weights: map[string]pnutconfig.RequestWeight{"GET": 0}}},
			expectedCode:  http.StatusInternalServerError,
			returnArgsExec: []interface{}{
				redis.NewIntResult(4, nil),                                // Mimics result from ZRemRangeByScore
				redis.NewIntResult(4, nil),                                // Mimics result from ZAdd
				redis.NewStringSliceResult(zrangeResultMalformatted, nil), // Mimics result from ZRange malformatted
				redis.NewBoolResult(true, nil),                            // Mimics result from Expire
			},
			adapterAuthHeaderKey: &headerKey,
		},
		// Test case 9: IP address as identifier.
		{
			name:          "IPAsIdentifier",
			authHeaderKey: "X-Forwarded-For",
			authHeader:    "192.168.1.1",
			urlPath:       "/valid",
			method:        "GET",
			timeout:       500 * time.Millisecond,
			routes:        map[*regexp.Regexp]*pnutconfig.Route{regexp.MustCompile("/valid"): {Weights: map[string]pnutconfig.RequestWeight{"GET": 0}}},
			expectedCode:  http.StatusOK,
			returnArgsExec: []interface{}{
				redis.NewIntResult(4, nil),                             // Mimics result from ZRemRangeByScore
				redis.NewIntResult(4, nil),                             // Mimics result from ZAdd
				redis.NewStringSliceResult(zrangeResultsSumEight, nil), // Mimics result from ZRange malformatted
				redis.NewBoolResult(true, nil),                         // Mimics result from Expire
			},
			adapterAuthHeaderKey: nil,
		},
		// Test case 9: Header Key is empty.
		{
			name:          "EmptyHeaderKey",
			authHeaderKey: headerKey,
			authHeader:    "header_value",
			urlPath:       "/valid",
			method:        "GET",
			timeout:       500 * time.Millisecond,
			routes:        map[*regexp.Regexp]*pnutconfig.Route{regexp.MustCompile("/valid"): {Weights: map[string]pnutconfig.RequestWeight{"GET": 0}}},
			expectedCode:  http.StatusOK,
			returnArgsExec: []interface{}{
				redis.NewIntResult(4, nil),
				redis.NewIntResult(4, nil),
				redis.NewStringSliceResult(zrangeResultsSumEight, nil),
				redis.NewBoolResult(true, nil),
			},
			adapterAuthHeaderKey: &emptyHeaderKey,
		},
	}

	for _, tc := range testCases {
		suite.Run(tc.name, func() {
			// ----------------------------------------
			// SETUP
			// ----------------------------------------

			rq := require.New(suite.T())
			suite.mockRedisClient = mocks.NewRedisClient(suite.T())
			suite.mockPipeliner = mocks.NewPipeliner(suite.T())
			suite.adapter = NewAdapter(suite.logger, suite.responder, suite.mockRedisClient, tc.timeout, &sync.RWMutex{}, tc.routes, maxDailyScore, groupID, tc.adapterAuthHeaderKey)
			mw := suite.adapter.Engage(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {}))

			// ----------------------------------------
			// EXPECTATIONS
			// ----------------------------------------

			suite.mockRedisClient.On("Pipeline").Return(suite.mockPipeliner).Maybe()
			suite.mockPipeliner.On("ZRemRangeByScore", mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(nil).Maybe()
			suite.mockPipeliner.On("ZAdd", mock.Anything, mock.Anything, mock.Anything).Return(nil).Maybe()
			suite.mockPipeliner.On("ZRange", mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(nil).Maybe()
			suite.mockPipeliner.On("Expire", mock.Anything, mock.Anything, mock.Anything).Return(nil).Maybe()
			suite.mockPipeliner.On("Exec", mock.Anything).Return(castToCmderSlice(tc.returnArgsExec...), nil).Maybe()

			// ----------------------------------------
			// EXECUTION
			// ----------------------------------------

			// Define the request.
			req := httptest.NewRequest(tc.method, tc.urlPath, nil)
			req.Header.Set(tc.authHeaderKey, tc.authHeader)

			// Set the recorder.
			w := httptest.NewRecorder()

			// Test the getters.
			_ = suite.adapter.GetTimeout()
			_ = suite.adapter.GetMaxDailyScore()
			_ = suite.adapter.GetHeaderKey()
			_ = suite.adapter.GetRateLimitingMode()

			// Serve.
			mw.ServeHTTP(w, req)

			// ----------------------------------------
			// ASSERTIONS
			// ----------------------------------------

			rq.Equal(tc.expectedCode, w.Code)
			suite.mockRedisClient.AssertExpectations(suite.T())
			suite.mockPipeliner.AssertExpectations(suite.T())
		})
	}
}

// castToCmderSlice casts a slice of arguments to a slice of Cmder.
func castToCmderSlice(args ...interface{}) []redis.Cmder {
	// Define a castToCmderSlice
	cmderSlice := make([]redis.Cmder, len(args))

	// Iterate and cast.
	for i, arg := range args {
		cmderSlice[i] = arg.(redis.Cmder)
	}

	return cmderSlice
}
