package limiter

import (
	"context"
	"fmt"
	"net"
	"net/http"
	"regexp"
	"strconv"
	"strings"
	"sync"
	"time"

	"github.com/redis/go-redis/v9"
	"gitlab.com/fruitygo/gojuice/juice/logjuice"
	"gitlab.com/fruitygo/pnutmux/contract"
	"gitlab.com/fruitygo/pnutmux/pnutconfig"
)

const (
	MsgNotApplicable = "Not Applicable"
	MsgIpBased       = "IP Based"
	MsgHeaderBased   = "Header Based"
)

type Adapter struct {
	logger        contract.Logger
	responder     contract.Responder
	client        contract.RedisClient
	maxDailyScore uint
	groupID       string
	headerKey     *string
	pnutRouterMu  *sync.RWMutex
	routes        map[*regexp.Regexp]*pnutconfig.Route
	timeout       time.Duration
}

func NewAdapter(logger contract.Logger, responder contract.Responder, client contract.RedisClient, timeout time.Duration, pnutRouterMu *sync.RWMutex, routes map[*regexp.Regexp]*pnutconfig.Route, maxDailyScore uint, groupID string, headerKey *string) *Adapter {
	return &Adapter{
		logger:        logger,
		responder:     responder,
		client:        client,
		timeout:       timeout,
		pnutRouterMu:  pnutRouterMu,
		routes:        routes,
		maxDailyScore: maxDailyScore,
		groupID:       groupID,
		headerKey:     headerKey,
	}
}

// Engage is a middleware that checks if the caller has exceeded the daily limit.
func (a *Adapter) Engage(next http.HandlerFunc) http.HandlerFunc {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		// Lock mutex.
		a.pnutRouterMu.RLock()
		defer a.pnutRouterMu.RUnlock()

		// Define a context and defer cancel.
		ctx, cancel := context.WithTimeout(r.Context(), a.timeout)
		defer cancel()

		// Get the current epoch in milliseconds.
		epochMs := float64(time.Now().UnixNano() / int64(time.Millisecond))

		// Define the identifier.
		identifier := ""

		// Retrieve the appropriate identifier and fallback to the IP address if the header is not set.
		if a.headerKey == nil {
			identifier = getClientIP(r)
		} else {
			if v := r.Header.Get(*a.headerKey); v == "" {
				identifier = getClientIP(r)
			} else {
				identifier = v
			}
		}

		// Define the weight.
		var weight pnutconfig.RequestWeight

		// Define a flag to check if the route was matched.
		matched := false

	loop:
		for regex, route := range a.routes {
			// Logging.
			a.logger.SLog(ctx, logjuice.Debug, "📖", "Registered regex is", regex.String())
			a.logger.SLog(ctx, logjuice.Debug, "🔍", "Method is", r.Method)
			a.logger.SLog(ctx, logjuice.Debug, "🔍", fmt.Sprintf("Weight is %d", route.Weights[r.Method]))

			if regex.MatchString(r.URL.Path) {
				// Log the regex match.
				a.logger.SLog(ctx, logjuice.Debug, "🔓", "URL.Path", r.URL.Path, "matched regex", regex.String())

				// Find the weight for the method and the route.
				if w, ok := route.Weights[r.Method]; ok {
					weight = w
					matched = true
					break loop
				}

				// Log the missing weight if it was not found.
				a.logger.SLog(ctx, logjuice.Error, "", "caller attempted to access", r.URL.Path, "with method", r.Method, "but no weight was found")
				a.responder.WriteErrorResponse(w, http.StatusInternalServerError)
				return
			}

			// Log the regex mismatch.
			a.logger.SLog(ctx, logjuice.Debug, "🔐", "URL.Path", r.URL.Path, "did not match regex", regex.String())
		}

		// If no route was found, return an error.
		if !matched {
			a.logger.SLog(ctx, logjuice.Error, "", "caller attempted to access", r.URL.Path, "with method", r.Method, "but no route was found")
			a.responder.WriteErrorResponse(w, http.StatusInternalServerError)
			return
		}

		// Redis key definition.
		key := fmt.Sprintf("%s:%s:daily", a.groupID, identifier)

		// Log the key generation.
		a.logger.SLog(ctx, logjuice.Debug, "🔑", "Redis key successfully generated")

		// response is a struct that holds the response from the pipeline.
		type response struct {
			error  error
			cmders []redis.Cmder
		}

		// Make a channel to receive the response from the pipeline since call is asynchronous.
		respChan := make(chan response)

		// Define a struct to hold the response from the pipeline.
		var redisResponse response

		go func() {
			defer close(respChan)

			// Define the pipeline.
			pipe := a.client.Pipeline()
			pipe.ZRemRangeByScore(ctx, key, "-inf", fmt.Sprintf("%f", epochMs-pnutconfig.OneDayInMs))
			pipe.ZAdd(ctx, key, redis.Z{Score: epochMs, Member: fmt.Sprintf("%f:%d", epochMs, weight)})
			pipe.ZRange(ctx, key, 0, -1)
			pipe.Expire(ctx, key, 24*time.Hour)

			// Execute the pipeline.
			res, err := pipe.Exec(ctx)

			// Send the response to the channel.
			respChan <- response{
				cmders: res,
				error:  err,
			}
		}()

		// Launch a select statement to handle the timeout.
		select {
		case <-ctx.Done():
			a.logger.SLog(ctx, logjuice.Error, "", fmt.Sprintf("caller exceeded timeout of %d ms", a.timeout.Milliseconds()))
			a.responder.WriteErrorResponse(w, http.StatusRequestTimeout)
			return
		case res := <-respChan:
			redisResponse = res
		}

		// Define the dayScore.
		var dayScore uint

		// Attempt assertion.
		cmd, ok := redisResponse.cmders[2].(*redis.StringSliceCmd)
		if !ok {
			a.logger.SLog(ctx, logjuice.Error, "", "failed to assert redisResponse.cmders[2] to redis.StringSliceCmd")
			a.responder.WriteErrorResponse(w, http.StatusInternalServerError)
			return
		}

		// [2] is the ZRange command, since the []Cmder is in the same order as the commands in the pipeline.
		for _, val := range cmd.Val() {
			parts := strings.Split(val, ":")

			// Retrieve the last part of the string, which is the weight.
			score, err := strconv.Atoi(parts[len(parts)-1])
			if err != nil {
				a.logger.SLog(ctx, logjuice.Error, "", "caller attempted to access", r.URL.Path, "with method", r.Method, "but the weight was not an integer")
				a.responder.WriteErrorResponse(w, http.StatusInternalServerError)
				return
			}

			// Add the score to the dayScore.
			dayScore += uint(score)
		}

		// Validate the dayScore.
		if dayScore > a.maxDailyScore {
			a.logger.SLog(ctx, logjuice.Warn, "✋", fmt.Sprintf("Caller exceeded daily allowed score of %d", a.maxDailyScore))
			a.responder.WriteErrorResponse(w, http.StatusTooManyRequests)
			return
		}

		next.ServeHTTP(w, r)
	})
}

// getClientIP extracts the client IP address from the request.
func getClientIP(r *http.Request) string {
	for _, header := range pnutconfig.IPHeaders {
		ips := r.Header.Get(header)

		if ips != "" {
			// Some headers may contain multiple IP addresses, but the first valid one is considered.
			for _, ip := range strings.Split(ips, ",") {
				ip = strings.TrimSpace(ip)

				// Parse IP.
				if net.ParseIP(ip) != nil {
					return ip
				}
			}
		}
	}

	// If no valid IP was found in the headers, extract it from r.RemoteAddr.
	host, _, err := net.SplitHostPort(r.RemoteAddr)
	if err != nil {
		// If splitting fails, return the whole RemoteAddr.
		return r.RemoteAddr
	}

	return host
}

// ------------------------------
// GETTERS
// ------------------------------

// GetMaxDailyScore returns the maximum daily score.
func (a *Adapter) GetMaxDailyScore() uint {
	return a.maxDailyScore
}

// GetTimeout returns the timeout.
func (a *Adapter) GetTimeout() string {
	return a.timeout.String()
}

// GetHeaderKey returns the header key.
func (a *Adapter) GetHeaderKey() string {
	if a.headerKey == nil {
		return MsgNotApplicable
	}

	if *a.headerKey == "" {
		return MsgNotApplicable
	}

	return *a.headerKey
}

// GetRateLimitingMode returns the rate limiting mode.
func (a *Adapter) GetRateLimitingMode() string {
	if a.headerKey == nil {
		return MsgIpBased
	}

	if *a.headerKey == "" {
		return MsgIpBased
	}

	return MsgHeaderBased
}
