// Package logger is a package that provides a logger interface and an adapter for the slog package.
package logger

import (
	"context"
	"io"
	"log/slog"

	"gitlab.com/fruitygo/gojuice/juice/logjuice"
)

type Adapter struct {
	logger logjuice.LoggerPort
}

func NewAdapter(out io.Writer, level slog.Level, traceKey interface{}) *Adapter {
	return &Adapter{
		logger: logjuice.NewLogger(out, level, traceKey),
	}
}

// Info logs an info message.
func (l *Adapter) Info(entry logjuice.GoogleLogEntry) {
	l.logger.Info(entry)
}

// Debug logs a debug message.
func (l *Adapter) Debug(entry logjuice.GoogleLogEntry) {
	l.logger.Debug(entry)
}

// Error logs an error message.
func (l *Adapter) Error(entry logjuice.GoogleLogEntry) {
	l.logger.Error(entry)
}

// Warn logs a warning message.
func (l *Adapter) Warn(entry logjuice.GoogleLogEntry) {
	l.logger.Warn(entry)
}

// Log logs a message.
func (l *Adapter) Log(ctx context.Context, level logjuice.LogLevel, message, component string) {
	l.logger.Log(ctx, level, message, component)
}

// SLog logs a structured message with the specified level, emoji & messages, in a structured way following Google Cloud's logging guidelines.
//
// Trace is retrieved from the context using the logger's trace key.
func (l *Adapter) SLog(ctx context.Context, level logjuice.LogLevel, emoji string, message ...string) {
	l.logger.SLog(ctx, level, emoji, message...)
}

// JSONLog logs a structured message with the specified level, emoji & messages, in a structured way following Google Cloud's logging guidelines.
//
// The data parameter is expected to be a struct or map that can be marshaled into JSON.
//
// Message field will be populated with the indent JSON string of the data.
//
// If the data is nil, the message field will be populated with a message indicating that the data is nil.
//
// If the data cannot be indented into a JSON string, the message field will be populated with a message indicating that the data could not be indented.
//
// Trace is retrieved from the context using the logger's trace key.
func (l *Adapter) JSONLog(ctx context.Context, level logjuice.LogLevel, data interface{}, emoji string, message ...string) {
	l.logger.JSONLog(ctx, level, data, emoji, message...)
}
