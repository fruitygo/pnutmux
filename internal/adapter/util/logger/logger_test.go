package logger

import (
	"bytes"
	"context"
	"log/slog"
	"strings"
	"testing"

	"gitlab.com/fruitygo/gojuice/juice/logjuice"
)

func TestBasicLogMethods(t *testing.T) {
	// Define a buffer.
	buf := &bytes.Buffer{}

	// Instanciate the adapter.
	adapter := NewAdapter(buf, slog.LevelDebug, nil)

	// Define a log entry.
	entry := logjuice.GoogleLogEntry{
		Component: "testComponent",
		Trace:     "testTrace",
		Message:   "testMessage",
	}

	// Define test cases.
	tests := []struct {
		name      string
		logFunc   func(logjuice.GoogleLogEntry)
		wantLevel string
	}{
		{"Info", adapter.Info, "INFO"},
		{"Debug", adapter.Debug, "DEBUG"},
		{"Error", adapter.Error, "ERROR"},
		{"Warn", adapter.Warn, "WARN"},
	}

	// Iterate cases and test.
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			// Reset the buffer.
			buf.Reset()

			// Call log function.
			tt.logFunc(entry)

			// Verify content of the log.
			if !strings.Contains(buf.String(), tt.wantLevel) || !strings.Contains(buf.String(), entry.Message) {
				t.Errorf("%s log does not contain expected level or message", tt.name)
			}
		})
	}
}

func TestSLog(t *testing.T) {
	// Define a buffer.
	buf := &bytes.Buffer{}

	// Instanciate the adapter.
	adapter := NewAdapter(buf, slog.LevelDebug, nil)

	// Define a log entry.
	tests := []struct {
		name      string
		logFunc   func(ctx context.Context, level logjuice.LogLevel, emoji string, message ...string)
		wantLevel string
	}{
		{
			name:      "SLog, Info",
			logFunc:   adapter.SLog,
			wantLevel: "INFO",
		},
	}

	// Iterate cases and test.
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			// Reset the buffer.
			buf.Reset()

			// Define a context.
			ctx := context.Background()

			// Call log function.
			tt.logFunc(ctx, logjuice.Info, "🚀", "testMessage", "anotherMessage", "and another message!")

			// Verify content of the log.
			if !strings.Contains(buf.String(), tt.wantLevel) || !strings.Contains(buf.String(), "testMessage") {
				t.Errorf("%s log does not contain expected level or message", tt.name)
			}
		})
	}
}

func TestJSONLog(t *testing.T) {
	// Define a buffer.
	buf := &bytes.Buffer{}

	// Instanciate the adapter.
	adapter := NewAdapter(buf, slog.LevelDebug, nil)

	// Define test cases.
	tests := []struct {
		name      string
		logFunc   func(ctx context.Context, level logjuice.LogLevel, data interface{}, emoji string, message ...string)
		wantLevel string
	}{
		{
			name:      "JSONLog, Info",
			logFunc:   adapter.JSONLog,
			wantLevel: "INFO",
		},
	}

	// Iterate cases and test.
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			// Reset the buffer.
			buf.Reset()

			// Define a context.
			ctx := context.Background()

			// Define data.
			data := map[string]interface{}{"testKey": "testValue"}

			// Call log function.
			tt.logFunc(ctx, logjuice.Info, data, "", "testMessage", "anotherMessage", "yetAnotherMessage")

			// Verify content of the log.
			if !strings.Contains(buf.String(), tt.wantLevel) || !strings.Contains(buf.String(), "testMessage") {
				t.Errorf("%s log does not contain expected level or message", tt.name)
			}
		})
	}
}

func TestLog(t *testing.T) {
	// Define a buffer.
	buf := &bytes.Buffer{}

	// Instanciate the adapter.
	adapter := NewAdapter(buf, slog.LevelDebug, nil)

	// Define test cases.
	tests := []struct {
		name      string
		logFunc   func(ctx context.Context, level logjuice.LogLevel, message, component string)
		wantLevel string
	}{
		{
			name:      "Log, Info",
			logFunc:   adapter.Log,
			wantLevel: "INFO",
		},
	}

	// Iterate cases and test.
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			// Reset the buffer.
			buf.Reset()

			// Define a context.
			ctx := context.Background()

			// Call log function.
			tt.logFunc(ctx, logjuice.Info, "testMessage", "testComponent")

			// Verify content of the log.
			if !strings.Contains(buf.String(), tt.wantLevel) || !strings.Contains(buf.String(), "testMessage") {
				t.Errorf("%s log does not contain expected level or message", tt.name)
			}
		})
	}
}
