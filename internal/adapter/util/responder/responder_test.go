package responder

import (
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestResponderWriteJSONResponse(t *testing.T) {
	// Instanciate the adapter.
	adapter := NewAdapter()

	// Define test cases.
	tests := []struct {
		name string
		data interface{}
		code int
	}{
		{"Success", map[string]string{"message": "test"}, http.StatusOK},
		{"Not Found", map[string]string{"message": "not found"}, http.StatusNotFound},
		{"Internal Server Error", map[string]string{"message": "internal server error"}, http.StatusInternalServerError},
	}

	// Iterate cases and test.
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			// Instanciate the recorder.
			w := httptest.NewRecorder()

			// Call the method.
			if err := adapter.WriteJSONResponse(w, tt.data, tt.code, false); err != nil {
				t.Errorf("unexpected error: %v", err)
			}

			// Retrieve the response.
			resp := w.Result()

			// Verify status code.
			if resp.StatusCode != tt.code {
				t.Errorf("expected status code %d, got %d", tt.code, resp.StatusCode)
			}
		})
	}
}

func TestResponderWriteErrorResponse(t *testing.T) {
	// Instanciate the adapter.
	adapter := NewAdapter()

	// Define test cases.
	tests := []struct {
		name      string
		errorCode int
	}{
		{"Error", http.StatusInternalServerError},
		{"Not Found", http.StatusNotFound},
		{"Bad Request", http.StatusBadRequest},
	}

	// Iterate cases and test.
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			// Instanciate the recorder.
			w := httptest.NewRecorder()

			// Call the method.
			adapter.WriteErrorResponse(w, tt.errorCode)

			// Retrieve the response.
			resp := w.Result()

			// Verify status code.
			if resp.StatusCode != tt.errorCode {
				t.Errorf("expected status code %d, got %d", tt.errorCode, resp.StatusCode)
			}
		})
	}
}
