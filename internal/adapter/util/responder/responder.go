// Package responder provides an adapter for writing responses to the response writer.
package responder

import (
	"net/http"

	"gitlab.com/fruitygo/gojuice/juice/apijuice"
)

type Adapter struct{}

func NewAdapter() *Adapter {
	return &Adapter{}
}

// WriteJSONResponse writes a JSON response to the response writer.
func (a *Adapter) WriteJSONResponse(w http.ResponseWriter, data interface{}, code int, escapeHTML bool) error {
	return apijuice.WriteJSONResponse(w, data, code, escapeHTML)
}

// WriteErrorResponse writes an error response to the response writer.
func (a *Adapter) WriteErrorResponse(w http.ResponseWriter, errorCode int) {
	apijuice.WriteErrorResponse(w, errorCode, false)
}
