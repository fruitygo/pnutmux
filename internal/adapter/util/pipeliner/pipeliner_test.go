package pipeliner

import (
	"context"
	"testing"

	"github.com/go-redis/redismock/v9"
	"github.com/redis/go-redis/v9"
	"github.com/stretchr/testify/assert"
)

func TestNewAdapter(t *testing.T) {
	// Mock the redis client.
	mockRedisClient, _ := redismock.NewClientMock()

	// Instanciate the pipeline.
	pipe := NewAdapter(mockRedisClient.Pipeline())

	// Assert.
	assert.NotNil(t, pipe)
}

func TestZRemRangeByScore(t *testing.T) {
	// Mock the redis client.
	mockRedisClient, _ := redismock.NewClientMock()

	// Instanciate the pipeline.
	pipe := NewAdapter(mockRedisClient.Pipeline())

	// Call the method.
	res := pipe.ZRemRangeByScore(context.TODO(), "key", "min", "max")

	// Assert.
	assert.NotNil(t, res)
}

func TestZAdd(t *testing.T) {
	// Mock the redis client.
	mockRedisClient, _ := redismock.NewClientMock()

	// Instanciate the pipeline.
	pipe := NewAdapter(mockRedisClient.Pipeline())

	// Call the method.
	res := pipe.ZAdd(context.TODO(), "key", redis.Z{})

	// Assert.
	assert.NotNil(t, res)
}

func TestZRange(t *testing.T) {
	// Mock the redis client.
	mockRedisClient, _ := redismock.NewClientMock()

	// Instanciate the pipeline.
	pipe := NewAdapter(mockRedisClient.Pipeline())

	// Call the method.
	res := pipe.ZRange(context.TODO(), "key", 0, -1)

	// Assert.
	assert.NotNil(t, res)
}

func TestExpire(t *testing.T) {
	// Mock the redis client.
	mockRedisClient, _ := redismock.NewClientMock()

	// Instanciate the pipeline.
	pipe := NewAdapter(mockRedisClient.Pipeline())

	// Call the method.
	res := pipe.Expire(context.TODO(), "key", 0)

	// Assert.
	assert.NotNil(t, res)
}

func TestExec(t *testing.T) {
	// Mock the redis client.
	mockRedisClient, _ := redismock.NewClientMock()

	// Instanciate the pipeline.
	pipe := NewAdapter(mockRedisClient.Pipeline())

	// Call the method.
	_, err := pipe.Exec(context.Background())

	// Assert.
	assert.Nil(t, err)
}
