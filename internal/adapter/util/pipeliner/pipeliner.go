package pipeliner

import (
	"context"
	"time"

	"github.com/redis/go-redis/v9"
)

type Adapter struct {
	pipeliner redis.Pipeliner
}

func NewAdapter(pipeliner redis.Pipeliner) *Adapter {
	return &Adapter{
		pipeliner: pipeliner,
	}
}

// ZRemRangeByScore removes all elements in the sorted set stored at key with a score between min and max (inclusive).
func (a *Adapter) ZRemRangeByScore(ctx context.Context, key string, min, max string) *redis.IntCmd {
	return a.pipeliner.ZRemRangeByScore(ctx, key, min, max)
}

// ZAdd adds all the specified members with the specified scores to the sorted set stored at key.
func (a *Adapter) ZAdd(ctx context.Context, key string, members ...redis.Z) *redis.IntCmd {
	return a.pipeliner.ZAdd(ctx, key, members...)
}

// ZRange returns the specified range of elements in the sorted set stored at key.
func (a *Adapter) ZRange(ctx context.Context, key string, start, stop int64) *redis.StringSliceCmd {
	return a.pipeliner.ZRange(ctx, key, start, stop)
}

// Expire sets a timeout on key. After the timeout has expired, the key will automatically be deleted.
func (a *Adapter) Expire(ctx context.Context, key string, expiration time.Duration) *redis.BoolCmd {
	return a.pipeliner.Expire(ctx, key, expiration)
}

// Exec executes all previously queued commands in a transaction and restores the connection state to normal.
func (a *Adapter) Exec(ctx context.Context) ([]redis.Cmder, error) {
	return a.pipeliner.Exec(ctx)
}
