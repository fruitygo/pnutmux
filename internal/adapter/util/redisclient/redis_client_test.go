package redisclient

import (
	"testing"

	"github.com/go-redis/redismock/v9"
	"github.com/stretchr/testify/assert"
)

func TestNewAdapter(t *testing.T) {
	// Mock the redis client.
	mockRedisClient, _ := redismock.NewClientMock()

	// Instanciate the adapter.
	adapter := NewRedisClientAdapter(mockRedisClient)

	// Assert.
	assert.NotNil(t, adapter)
	assert.Equal(t, mockRedisClient, adapter.client)
}

func TestPipeline(t *testing.T) {
	// Mock the redis client.
	mockRedisClient, _ := redismock.NewClientMock()

	// Instanciate the adapter.
	adapter := NewRedisClientAdapter(mockRedisClient)

	// Instanciate the pipeline.
	pipe := adapter.Pipeline()

	// Assert.
	assert.NotNil(t, pipe)
}
