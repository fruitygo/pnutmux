package redisclient

import (
	"github.com/redis/go-redis/v9"
	"gitlab.com/fruitygo/pnutmux/contract"
	"gitlab.com/fruitygo/pnutmux/internal/adapter/util/pipeliner"
)

type Adapter struct {
	client *redis.Client
}

// NewRedisClientAdapter creates a new instance of the Adapter struct.
func NewRedisClientAdapter(client *redis.Client) *Adapter {
	return &Adapter{
		client: client,
	}
}

// Pipeline returns a new instance of the Pipeliner struct.
func (a *Adapter) Pipeline() contract.Pipeliner {
	return pipeliner.NewAdapter(a.client.Pipeline())
}
