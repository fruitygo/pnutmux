package pnutmux

import (
	"embed"
	"fmt"
	"log"
	"net/http"
	"net/http/httptest"
	"os"
	"reflect"
	"regexp"
	"sync"
	"syscall"
	"testing"
	"time"

	"github.com/go-redis/redismock/v9"
	"gitlab.com/fruitygo/gojuice/juice/testjuice"
	"gitlab.com/fruitygo/pnutmux/pnutconfig"
)

//go:embed embed/*
var testEmbed embed.FS

// TestNewRouter validates the behavior of the NewRouter function by verifying the type of the returned router.
// It ensures that the function returns an instance of *pnutRouter, indicating the correct creation of the router.
func TestNewRouter(t *testing.T) {
	// Define the expected router.
	result := NewRouter(semCount, nil, nil)

	// Compare the result with the expected value.
	_, ok := result.(*PnutRouter)
	if !ok {
		t.Errorf("NewRouter() returned a value of type %T, expected *pnutRouter", result)
	}
}

func TestNewRouterWithRedis(t *testing.T) {
	// Define a mock redis client.
	mockRedisClient, _ := redismock.NewClientMock()

	// Define rate limiting parameters.
	rp := &RateLimitingParams{
		RedisClient:   mockRedisClient,
		RedisTimeout:  5 * time.Second,
		MaxDailyScore: 10,
		GroupID:       "test",
	}

	// Define the expected router.
	result := NewRouterWithRedis(semCount, nil, rp, nil)

	// Compare the result with the expected value.
	_, ok := result.(*PnutRouter)
	if !ok {
		t.Errorf("NewRouterWithRedis() returned a value of type %T, expected *pnutRouter", result)
	}
}

func TestNewRouterWithRedisAndTLS(t *testing.T) {
	// Define a mock redis client.
	mockRedisClient, _ := redismock.NewClientMock()

	// Define rate limiting parameters.
	rp := &RateLimitingParams{
		RedisClient:   mockRedisClient,
		RedisTimeout:  5 * time.Second,
		MaxDailyScore: 10,
		GroupID:       "test",
	}

	// Define http2 configuration.
	http2Config := &HTTP2Config{
		FS:              testEmbed,
		CertificateFile: "embed/test_server.crt",
		KeyFile:         "embed/test_server.key",
	}

	// Define the expected router.
	result := NewRouterWithRedis(semCount, nil, rp, http2Config)

	// Compare the result with the expected value.
	_, ok := result.(*PnutRouter)
	if !ok {
		t.Errorf("NewRouterWithRedis() returned a value of type %T, expected *pnutRouter", result)
	}
}

func TestNewRouterWithRedisAndH2C(t *testing.T) {
	// Define a mock redis client.
	mockRedisClient, _ := redismock.NewClientMock()

	// Define rate limiting parameters.
	rp := &RateLimitingParams{
		RedisClient:   mockRedisClient,
		RedisTimeout:  5 * time.Second,
		MaxDailyScore: 10,
		GroupID:       "test",
	}

	// Define http2 configuration.
	http2Config := &HTTP2Config{
		UseH2C: true,
	}

	// Define the expected router.
	result := NewRouterWithRedis(semCount, nil, rp, http2Config)

	// Compare the result with the expected value.
	_, ok := result.(*PnutRouter)
	if !ok {
		t.Errorf("NewRouterWithRedis() returned a value of type %T, expected *pnutRouter", result)
	}
}

func TestNewRouterWithNilRedisClient(t *testing.T) {
	// Define rate limiting parameters.
	rp := &RateLimitingParams{
		RedisClient:   nil,
		RedisTimeout:  5 * time.Second,
		MaxDailyScore: 10,
		GroupID:       "test",
	}

	// Define the expected router.
	result := NewRouterWithRedis(semCount, nil, rp, nil)

	// Compare the result with the expected value.
	_, ok := result.(*PnutRouter)
	if !ok {
		t.Errorf("NewRouterWithRedis() returned a value of type %T, expected *pnutRouter", result)
	}
}

func TestNewRouterWithNilRateLimitingParams(t *testing.T) {
	// Define the expected router.
	result := NewRouterWithRedis(semCount, nil, nil, nil)

	// Compare the result with the expected value.
	_, ok := result.(*PnutRouter)
	if !ok {
		t.Errorf("NewRouterWithRedis() returned a value of type %T, expected *pnutRouter", result)
	}
}

// TestSetNotFoundHandler validates the behavior of the SetNotFoundHandler function by setting a custom not found handler
// and verifying that it has been set correctly on the router. It also checks if the signature of the custom not found handler
// matches the one stored in the router.
func TestSetNotFoundHandler(t *testing.T) {
	// Create a new router.
	router := NewRouter(semCount, nil, nil)

	// Create a custom not found handler.
	customnotfoundhandler := func(w http.ResponseWriter, r *http.Request) {
		http.Error(w, "custom not found", http.StatusNotFound)
	}

	// Set the custom not found handler.
	router.SetNotFoundHandler(customnotfoundhandler)

	// Verify that the not found handler has been set correctly.
	if router.(*PnutRouter).notFoundHandler == nil {
		t.Errorf("notfoundhandler is nil")
	}

	// Check if the signature of the custom not found handler and the one in the router are the same.
	expectedType := reflect.TypeOf((http.HandlerFunc)(nil))
	actualType := reflect.TypeOf(router.(*PnutRouter).notFoundHandler)
	if actualType != expectedType {
		t.Errorf("expected custom not found handler with signature %v, got %v", expectedType, actualType)
	}
}

// TestRun validates the behavior of the Run function by starting a server in a separate goroutine,
// sending a test request to the server, and then stopping the server. It uses a wait group to synchronize
// the test and server shutdown. The test checks for any errors that occur during server startup, request
// sending, and server shutdown.
func TestRun(t *testing.T) {
	// Define a mock redis client.
	mockClient, _ := redismock.NewClientMock()

	// Define rate limiting parameters.
	rp := &RateLimitingParams{
		RedisClient:   mockClient,
		RedisTimeout:  5 * time.Second,
		MaxDailyScore: 10,
		GroupID:       "test",
	}

	// Define test cases.
	testCases := []struct {
		name        string
		http2Config *HTTP2Config
	}{
		{
			name: "UseH2C: true",
			http2Config: &HTTP2Config{
				UseH2C:          true,
				FS:              testEmbed,
				CertificateFile: "embed/test_server.crt",
				KeyFile:         "embed/test_server.key",
			},
		},
		{
			name: "UseH2C: false",
			http2Config: &HTTP2Config{
				UseH2C:          false,
				FS:              testEmbed,
				CertificateFile: "embed/test_server.crt",
				KeyFile:         "embed/test_server.key",
			},
		},
	}

	// Iterate over the test cases.
	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			// Define a new router.
			router := NewRouterWithRedis(semCount, nil, rp, tc.http2Config)

			// Register multiple routes to implicitly test the ascii chart.
			router.Register(regexp.MustCompile("/v1/anything"), func(w http.ResponseWriter, r *http.Request) {}, Unlimited, http.MethodGet)
			router.Register(regexp.MustCompile("/v2/anything"), func(w http.ResponseWriter, r *http.Request) {}, Unlimited, http.MethodPost)

			// Create a wait group to synchronize the test and server shutdown.
			var wg sync.WaitGroup
			wg.Add(1)

			// Start the server in a separate goroutine.
			go func() {
				// Defer wait group done.
				defer wg.Done()

				err := router.Run("localhost:8000", 120*time.Second, 60*time.Second, 60*time.Second)
				if err != nil && err != http.ErrServerClosed {
					t.Errorf("Run failed: %v", err)
				}
			}()

			// Wait for the server to start.
			if err := testjuice.WaitForServer(localhost8000URL, waitForServerDelay); err != nil {
				t.Errorf("failed to wait for the server to start: %v", err)
			}

			// Send a test request to the server.
			_, err := http.Get("http://localhost:8000")
			if err != nil {
				t.Errorf("Run failed: %v", err)
			}

			// Call the Run method again and expect an error.
			err = router.Run("localhost:8000", 120*time.Second, 5*time.Second, 5*time.Second)
			expectedError := "server is already running"
			if err == nil || err.Error() != expectedError {
				t.Errorf("expected error: %s, got: %v", expectedError, err)
			}

			// Stop the server after the test has run.
			if err := router.Stop(); err != nil {
				t.Errorf("Shutdown failed: %v", err)
			}

			// Wait for the server to shut down before returning.
			wg.Wait()

			// Print a log message indicating that the test has completed.
			log.Print("TestRun completed")
		})
	}
}

// TestRun_StartupError is a test function that verifies the behavior of the Run method when encountering a startup error.
// It creates a new router and attempts to start the server with an invalid address.
// The test expects a startup error to be returned.
// After the test, it stops the server using the Stop method and checks for any shutdown errors.
func TestRun_StartupError(t *testing.T) {
	// Create a new router.
	router := NewRouter(semCount, nil, nil)

	// Start the server with an invalid address.
	err := router.Run("invalid_address", 120*time.Second, 60*time.Second, 60*time.Second)
	if err == nil {
		t.Error("expected startup error, got nil")
	}

	// Stop the server after the test has run.
	if err := router.Stop(); err != nil {
		t.Errorf("Shutdown failed: %v", err)
	}
}

// TestRun_StartupSuccess is a test function that verifies the behavior of the Run method when the server starts successfully.
// It creates a new router and simulates a successful server startup without starting a real server.
// The test expects the router to be stopped successfully using the Stop method, and verifies that the value sent to the 'done' channel is nil.
func TestRun_StartupSuccess(t *testing.T) {
	// Create a new router.
	router := NewRouter(semCount, nil, nil)

	// Create a channel to capture the value sent to done.
	done := make(chan error, 1)

	// Simulate a successful server startup without starting a real server.
	go func() {
		// Send a nil error to the done channel.
		done <- nil
	}()

	// Stop the server after the test has run.
	if err := router.Stop(); err != nil {
		t.Errorf("Shutdown failed: %v", err)
	}

	// Verify that nil is assigned to the done channel.
	err := <-done
	if err != nil {
		t.Errorf("expected nil, got: %v", err)
	}
}

// TestRun_SignalHandling is a test function that verifies the behavior of the Run method when handling signals for server shutdown.
// It creates a new router and starts the server in a separate goroutine. The test then sends a signal to trigger the server shutdown,
// waits for the server to shut down, and asserts that the server instance is reset to nil after shutdown.
func TestRun_SignalHandling(t *testing.T) {
	// Create a new router.
	router := NewRouter(semCount, nil, nil)

	// Create a wait group to synchronize the test and server shutdown.
	var wg sync.WaitGroup
	wg.Add(1)

	// Start the server in a separate goroutine
	go func() {
		// Defer wait group done.
		defer wg.Done()

		// Run.
		err := router.Run("localhost:8000", 120*time.Second, 60*time.Second, 60*time.Second)
		if err != nil && err != http.ErrServerClosed {
			t.Errorf("Run failed: %v", err)
		}
	}()

	// Wait for the server to start.
	if err := testjuice.WaitForServer(localhost8000URL, waitForServerDelay); err != nil {
		t.Errorf("failed to wait for the server to start: %v", err)
	}

	// Send a signal to trigger the server shutdown.
	syscall.Kill(os.Getpid(), syscall.SIGINT)

	// Wait for the server to shut down before returning.
	wg.Wait()

	// Assert the expected behavior.
	if router.(*PnutRouter).srv != nil {
		t.Error("expected server instance to be reset to nil after shutdown")
	}
}

// TestServeHTTP_StatusNotFound is a test function that verifies the behavior of the ServeHTTP method for handling a status not found (404) error.
// It defines a test case with a non-matching route, sets up the router with the test routes, sends a request to the router, and checks that the response status code is as expected.
func TestServeHTTP_StatusNotFound(t *testing.T) {
	// Define test cases.
	tests := []struct {
		name           string
		routes         map[*regexp.Regexp]*pnutconfig.Route
		url            string
		method         string
		expectedStatus int
	}{
		{
			name: "Non-matching route",
			routes: map[*regexp.Regexp]*pnutconfig.Route{
				regexp.MustCompile(`/users/(\d+)`): {
					Handlers: map[string]http.HandlerFunc{
						http.MethodGet: func(w http.ResponseWriter, r *http.Request) {
							fmt.Fprint(w, "User detail")
						},
					},
					Weights: map[string]pnutconfig.RequestWeight{
						http.MethodGet: Unlimited,
					},
				},
			},
			url:            "/posts/456",
			method:         http.MethodGet,
			expectedStatus: http.StatusNotFound,
		},
	}

	// Iterate test cases.
	for _, test := range tests {
		// Run.
		t.Run(test.name, func(t *testing.T) {
			// Define a new router and set not found handler to nil.
			router := NewRouter(semCount, nil, nil).(*PnutRouter)
			router.notFoundHandler = nil // Removing the default not found handler to test 404 error.
			router.routes = test.routes  // Register the routes from the test case.

			// Define the request.
			req, err := http.NewRequest(test.method, test.url, nil)
			if err != nil {
				t.Fatalf("failed to create request: %v", err)
			}

			// Define the recorder.
			rr := httptest.NewRecorder()

			// Serve.
			router.ServeHTTP(rr, req)

			// Verify the response status code.
			if rr.Code != test.expectedStatus {
				t.Errorf("expected %d, got %d", test.expectedStatus, rr.Code)
			}
		})
	}
}

// TestServeHTTP validates the behavior of the ServeHTTP method of the router by testing different scenarios.
// It creates a test router, registers routes from the test case, sends an HTTP request using the specified URL
// and method, and verifies the response status code against the expected status code.
func TestServeHTTP(t *testing.T) {
	// Define test cases.
	tests := []struct {
		name           string
		routes         map[*regexp.Regexp]*pnutconfig.Route
		url            string
		method         string
		expectedStatus int
	}{
		{
			name: "Matching route and method",
			routes: map[*regexp.Regexp]*pnutconfig.Route{
				regexp.MustCompile(`/users/(\d+)`): {
					Handlers: map[string]http.HandlerFunc{
						http.MethodGet: func(w http.ResponseWriter, r *http.Request) {
							fmt.Fprint(w, "User detail")
						},
					},
					Weights: map[string]pnutconfig.RequestWeight{
						http.MethodGet: Unlimited,
					},
				},
			},
			url:            "/users/123",
			method:         http.MethodGet,
			expectedStatus: http.StatusOK,
		},
		{
			name: "Matching route, wrong method",
			routes: map[*regexp.Regexp]*pnutconfig.Route{
				regexp.MustCompile(`/users/(\d+)`): {
					Handlers: map[string]http.HandlerFunc{
						http.MethodGet: func(w http.ResponseWriter, r *http.Request) {
							fmt.Fprint(w, "User detail")
						},
					},
					Weights: map[string]pnutconfig.RequestWeight{
						http.MethodGet: Unlimited,
					},
				},
			},
			url:            "/users/123",
			method:         http.MethodPost,
			expectedStatus: http.StatusMethodNotAllowed,
		},
		{
			name: "Non-matching route",
			routes: map[*regexp.Regexp]*pnutconfig.Route{
				regexp.MustCompile(`/users/(\d+)`): {
					Handlers: map[string]http.HandlerFunc{
						http.MethodGet: func(w http.ResponseWriter, r *http.Request) {
							fmt.Fprint(w, "User detail")
						},
					},
					Weights: map[string]pnutconfig.RequestWeight{
						http.MethodGet: Unlimited,
					},
				},
			},
			url:            "/posts/456",
			method:         http.MethodGet,
			expectedStatus: http.StatusNotFound,
		},
	}

	// Iterate test cases.
	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			// Define a router.
			router := NewRouter(semCount, nil, nil).(*PnutRouter)
			router.routes = test.routes // Register the routes from the test case.

			// Define the request.
			req, err := http.NewRequest(test.method, test.url, nil)
			if err != nil {
				t.Fatalf("failed to create request: %v", err)
			}

			// Define the recorder.
			rr := httptest.NewRecorder()

			// Serve.
			router.ServeHTTP(rr, req)

			// Verify the response status code.
			if rr.Code != test.expectedStatus {
				t.Errorf("unexpected status code. Expected %d, got %d", test.expectedStatus, rr.Code)
			}
		})
	}
}

func TestRegisterWithRedis(t *testing.T) {
	// Define a redis mock client.
	mockClient, _ := redismock.NewClientMock()

	// Define rate limiting parameters.
	rp := &RateLimitingParams{
		RedisClient:   mockClient,
		RedisTimeout:  5 * time.Second,
		MaxDailyScore: 10,
		GroupID:       "test",
	}

	// Define a new router.
	router := NewRouterWithRedis(semCount, nil, rp, nil)

	// Define test cases.
	tests := []struct {
		name          string
		reg           *regexp.Regexp
		handler       http.HandlerFunc
		methods       []string
		expectedError bool
		weight        pnutconfig.RequestWeight
	}{
		{
			name: "EngagingLimiting",
			reg:  regexp.MustCompile("^/api/posts$"),
			handler: func(w http.ResponseWriter, r *http.Request) {
				// Handle request
			},
			methods:       []string{"GET"},
			expectedError: false,
			weight:        2,
		},
		{
			name: "DuplicateHandlerWithRedis",
			reg:  regexp.MustCompile("^/api/posts$"),
			handler: func(w http.ResponseWriter, r *http.Request) {
				// Handle request
			},
			methods:       []string{"GET"},
			expectedError: true,
			weight:        2,
		},
		{
			name: "DuplicateRegexWithNewMethodAndNewHandler",
			reg:  regexp.MustCompile("^/api/posts$"),
			handler: func(w http.ResponseWriter, r *http.Request) {
				// Handle request
			},
			methods:       []string{"DELETE"},
			expectedError: false,
			weight:        1,
		},
	}

	// Iterate test cases.
	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			// Register route.
			err := router.Register(test.reg, test.handler, test.weight, test.methods...)

			if test.expectedError && err == nil {
				t.Errorf("expected error, but got nil")
			} else if !test.expectedError && err != nil {
				t.Errorf("unexpected error: %v", err)
			}
		})
	}
}

// TestRegister is a test function that verifies the behavior of the Register method in PnutRouter.
// It creates a new instance of PnutRouter and performs multiple test cases to register routes with different configurations.
func TestRegister(t *testing.T) {
	// Define a new router.
	router := NewRouter(semCount, nil, nil)

	// Define test cases.
	tests := []struct {
		name          string
		reg           *regexp.Regexp
		handler       http.HandlerFunc
		methods       []string
		expectedError bool
		weight        pnutconfig.RequestWeight
	}{
		{
			name: "EmptyMethods",
			reg:  regexp.MustCompile("^/EmptyMethods$"),
			handler: func(w http.ResponseWriter, r *http.Request) {
				// Handle request
			},
			methods:       []string{},
			expectedError: false,
			weight:        Unlimited,
		},
		{
			name: "ValidRoute",
			reg:  regexp.MustCompile("^/api/users$"),
			handler: func(w http.ResponseWriter, r *http.Request) {
				// Handle request
			},
			methods:       []string{"GET", "POST"},
			expectedError: false,
			weight:        Unlimited,
		},
		{
			name: "DuplicateRoute",
			reg:  regexp.MustCompile("^/api/users$"),
			handler: func(w http.ResponseWriter, r *http.Request) {
				// Handle request
			},
			methods:       []string{"GET", "POST"},
			expectedError: true,
			weight:        Unlimited,
		},
		{
			name: "InvalidHTTPMethod",
			reg:  regexp.MustCompile("^/api/posts$"),
			handler: func(w http.ResponseWriter, r *http.Request) {
				// Handle request
			},
			methods:       []string{"INVALID"},
			expectedError: true,
			weight:        Unlimited,
		},
		{
			name: "WeightNotUnlimitedWithNilRedisClient",
			reg:  regexp.MustCompile("^/api/posts$"),
			handler: func(w http.ResponseWriter, r *http.Request) {
				// Handle request
			},
			methods:       []string{"GET"},
			expectedError: true,
			weight:        99,
		},
	}

	// Iterate test cases.
	for _, test := range tests {
		// Run.
		t.Run(test.name, func(t *testing.T) {
			// Register route.
			err := router.Register(test.reg, test.handler, test.weight, test.methods...)

			if test.expectedError && err == nil {
				t.Errorf("expected error, but got nil")
			} else if !test.expectedError && err != nil {
				t.Errorf("unexpected error: %v", err)
			}
		})
	}
}

// TestRegister_Emptyroutes is a test function that verifies the behavior of the Register method in PnutRouter when the routes map is initially empty.
// It creates a new instance of PnutRouter, resets the routes map, and performs a test case to register a route.
func TestRegister_Emptyroutes(t *testing.T) {
	// Define a new router.
	router := NewRouter(semCount, nil, nil)

	// Reset routes map.
	router.(*PnutRouter).routes = nil

	// Check if routes map is initially empty.
	if router.(*PnutRouter).routes != nil {
		t.Errorf("expected routes map to be nil, but got a non-nil value")
	}

	// Create a test regular expression.
	reg := regexp.MustCompile("^/api/users$")

	// Register a route.
	err := router.Register(reg, func(w http.ResponseWriter, r *http.Request) {}, Unlimited, "GET")
	if err != nil {
		t.Errorf("unexpected error: %v", err)
	}

	// Verify if the routes map is initialized.
	if router.(*PnutRouter).routes == nil {
		t.Errorf("routes map not initialized")
	}

	// Verify if the route is registered.
	if _, exists := router.(*PnutRouter).routes[reg]; !exists {
		t.Errorf("route not registered for regex %s", reg.String())
	}
}

// TestStop is a test function that verifies the behavior of the Stop method in PnutRouter.
// It includes multiple test cases to cover scenarios when the server is not running or running.
func TestStop(t *testing.T) {
	// Define test cases.
	tests := []struct {
		name          string
		serverRunning bool
		expectedError bool
	}{
		{
			name:          "ServerNotRunning",
			serverRunning: false,
			expectedError: false,
		},
		{
			name:          "ServerRunning",
			serverRunning: true,
			expectedError: false,
		},
	}

	// Iterate test cases.
	for _, test := range tests {
		// Run.
		t.Run(test.name, func(t *testing.T) {
			// Create a new router.
			router := NewRouter(semCount, nil, nil)

			// Create a wait group to synchronize the test and server shutdown.
			var wg sync.WaitGroup

			// Start the server in a separate goroutine.
			go func() {
				// Defer wait group done.
				defer wg.Done()

				wg.Add(1)

				// Run server.
				err := router.Run("localhost:8000", 120*time.Second, 5*time.Second, 5*time.Second)
				if err != nil && err != http.ErrServerClosed {
					t.Errorf("Run failed: %v", err)
				}
			}()

			// Wait for the server to start.
			if err := testjuice.WaitForServer(localhost8000URL, waitForServerDelay); err != nil {
				t.Errorf("failed to wait for the server to start: %v", err)
			}

			// Send a test request to the server.
			_, err := http.Get("http://localhost:8000")
			if err != nil {
				t.Errorf("Run failed: %v", err)
			}

			// Stop the server after the test has run.
			if err := router.Stop(); err != nil {
				t.Errorf("Shutdown failed: %v", err)
			}

			// Wait for the server to shut down before returning.
			wg.Wait()

			// Check the expected error.
			if test.expectedError && err == nil {
				t.Errorf("expected error, but got nil")
			} else if !test.expectedError && err != nil {
				t.Errorf("unexpected error: %v", err)
			}
		})
	}
}

// TestStop_ShutdownError is a test function that verifies the behavior of the Stop method in PnutRouter
// when an error occurs during server shutdown.
func TestStop_ShutdownError(t *testing.T) {
	// Define a mock server.
	mockServer := &serverwrapper{}

	// Define a new router.
	router := NewRouter(semCount, nil, nil)

	// Assign mock server to the router.
	router.(*PnutRouter).srv = mockServer

	// Test the Stop() function without starting the server.
	err := router.Stop()

	// Assert the expected behavior.
	if err == nil {
		t.Errorf("expected error, but got nil")
	}
}
