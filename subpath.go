package pnutmux

import "gitlab.com/fruitygo/pnutmux/pnutconfig"

// Subpath returns a string representing a subpath. Currently, it returns the static string "subpath".
func Subpath() string {
	return pnutconfig.SubpathKey
}
