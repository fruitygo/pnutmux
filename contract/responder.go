package contract

import (
	"net/http"
)

// ResponseWriter is an interface that defines the methods for a response writer.
type Responder interface {
	WriteJSONResponse(w http.ResponseWriter, data interface{}, code int, escapeHTML bool) error
	WriteErrorResponse(w http.ResponseWriter, code int)
}
