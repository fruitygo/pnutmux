package contract

import "context"

// Server is an interface that defines the methods for a server.
type Server interface {
	Shutdown(context.Context) error
	ListenAndServe() error
	ListenAndServeHTTP2() error
}
