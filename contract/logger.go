package contract

import (
	"context"

	"gitlab.com/fruitygo/gojuice/juice/logjuice"
)

// Logger is an interface that defines the methods for a logger.
type Logger interface {
	Info(entry logjuice.GoogleLogEntry)
	Error(entry logjuice.GoogleLogEntry)
	Debug(entry logjuice.GoogleLogEntry)
	Warn(entry logjuice.GoogleLogEntry)
	Log(ctx context.Context, level logjuice.LogLevel, message, component string)
	SLog(ctx context.Context, level logjuice.LogLevel, emoji string, messages ...string)
	JSONLog(ctx context.Context, level logjuice.LogLevel, data interface{}, emoji string, message ...string)
}
