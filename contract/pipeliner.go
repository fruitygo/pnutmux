package contract

import (
	"context"
	"time"

	"github.com/redis/go-redis/v9"
)

// Pipeline is an interface that defines the methods for a Redis pipeline.
type Pipeliner interface {
	ZRemRangeByScore(ctx context.Context, key string, min, max string) *redis.IntCmd
	ZAdd(ctx context.Context, key string, members ...redis.Z) *redis.IntCmd
	ZRange(ctx context.Context, key string, start, stop int64) *redis.StringSliceCmd
	Expire(ctx context.Context, key string, expiration time.Duration) *redis.BoolCmd
	Exec(ctx context.Context) ([]redis.Cmder, error)
}
