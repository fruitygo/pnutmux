package contract

import "net/http"

// Limiter is an interface that defines the methods for a rate limiter.
type Limiter interface {
	Engage(next http.HandlerFunc) http.HandlerFunc
	GetMaxDailyScore() uint
	GetTimeout() string
	GetHeaderKey() string
	GetRateLimitingMode() string
}
