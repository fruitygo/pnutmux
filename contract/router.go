package contract

import (
	"net/http"
	"regexp"
	"time"

	"gitlab.com/fruitygo/pnutmux/pnutconfig"
)

// Router is an interface that defines the methods for a router.
type Router interface {
	Run(addr string, idleTimeout, readTimeout, writeTimeout time.Duration) error
	Register(reg *regexp.Regexp, handler http.HandlerFunc, weight pnutconfig.RequestWeight, methods ...string) error
	SetNotFoundHandler(handler http.HandlerFunc)
	Stop() error
	ServeHTTP(w http.ResponseWriter, req *http.Request)
}
