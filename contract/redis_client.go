package contract

// RedisClient is an interface that defines the methods for a Redis client.
type RedisClient interface {
	Pipeline() Pipeliner
}
