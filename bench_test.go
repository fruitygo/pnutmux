package pnutmux

import (
	"net/http"
	"net/http/httptest"
	"regexp"
	"testing"
)

const (
	routePattern = "/v1/anything"
	httpMethod   = http.MethodGet
)

func BenchmarkServeHTTP(b *testing.B) {
	// Initialize the router.
	router := NewRouter(semCount, nil, nil)

	// Define and register the handler.
	handler := func(w http.ResponseWriter, r *http.Request) {}

	// Register the route.
	router.Register(regexp.MustCompile(routePattern), handler, Unlimited, httpMethod)

	// Create the request.
	request, err := http.NewRequest(httpMethod, routePattern, nil)
	if err != nil {
		b.Fatalf("failed to create request: %v", err)
	}

	// Reset the timer before running the benchmark loop.
	b.ResetTimer()

	// Run the loop.
	for i := 0; i < b.N; i++ {
		recorder := httptest.NewRecorder()

		router.ServeHTTP(recorder, request)

		if recorder.Code != http.StatusOK {
			b.Errorf("expected status OK; got %v", recorder.Code)
		}
	}
}
