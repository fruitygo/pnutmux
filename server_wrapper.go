package pnutmux

import (
	"context"
	"crypto/tls"
	"errors"
	"fmt"
	"net/http"
	"time"

	"gitlab.com/fruitygo/pnutmux/pnutconfig"
	"golang.org/x/net/http2"
	"golang.org/x/net/http2/h2c"
)

// serverwrapper implements the contract/server.go server interface
type serverwrapper struct {
	*http.Server
}

// NewServerWrapper creates a new serverwrapper instance.
func NewServerWrapper(addr string, handler http.Handler, idleTimeout, readTimeout, writeTimeout time.Duration) *serverwrapper {
	return &serverwrapper{
		Server: &http.Server{
			Addr:         addr,
			Handler:      cors(handler),
			IdleTimeout:  idleTimeout,
			ReadTimeout:  readTimeout,
			WriteTimeout: writeTimeout,
		},
	}
}

// NewHTTP2ServerWrapper creates a new serverwrapper instance with http/2 enabled.
func NewHTTP2ServerWrapper(addr string, handler http.Handler, idleTimeout, readTimeout, writeTimeout time.Duration, http2Config *HTTP2Config) (*serverwrapper, error) {
	if http2Config.UseH2C {
		h2s := &http2.Server{}
		h1s := &http.Server{
			Addr:         addr,
			Handler:      h2c.NewHandler(cors(handler), h2s),
			IdleTimeout:  idleTimeout,
			ReadTimeout:  readTimeout,
			WriteTimeout: writeTimeout,
		}

		return &serverwrapper{Server: h1s}, nil
	}

	// Read the certificate and key files from the embedded file system.
	certData, err := http2Config.FS.ReadFile(http2Config.CertificateFile)
	if err != nil {
		return nil, fmt.Errorf("failed to read certificate file: %w", err)
	}

	// Read the key file from the embedded file system.
	keyData, err := http2Config.FS.ReadFile(http2Config.KeyFile)
	if err != nil {
		return nil, fmt.Errorf("failed to read key file: %w", err)
	}

	// Load the certificate and key.
	cert, err := tls.X509KeyPair(certData, keyData)
	if err != nil {
		return nil, fmt.Errorf("failed to load X509 key pair: %w", err)
	}

	// Get the base configuration for the HTTP/2 server.
	baseConf := pnutconfig.GetHTTP2Config()

	// Set the certificates.
	baseConf.Certificates = []tls.Certificate{cert}

	// Return the serverwrapper instance with the TLS configuration.
	return &serverwrapper{
		Server: &http.Server{
			Addr:         addr,
			Handler:      cors(handler),
			IdleTimeout:  idleTimeout,
			ReadTimeout:  readTimeout,
			WriteTimeout: writeTimeout,
			TLSConfig:    baseConf,
		},
	}, nil
}

// ListenAndServe simulates the ListenAndServe method of the serverwrapper.
func (sw *serverwrapper) ListenAndServe() error {
	if sw.Server != nil {
		return sw.Server.ListenAndServe()
	}

	return errors.New("server is nil while trying to listen and serve")
}

// ListenAndServeHTTP2 simulates the ListenAndServeHTTP2 method of the serverwrapper.
func (sw *serverwrapper) ListenAndServeHTTP2() error {
	if sw.Server != nil {
		// Pass empty strings to use the certificates in the TLSConfig.
		return sw.Server.ListenAndServeTLS("", "")
	}

	return errors.New("server is nil while trying to listen and serve")
}

// Shutdown simulates the Shutdown method of the serverwrapper.
func (sw *serverwrapper) Shutdown(ctx context.Context) error {
	if sw.Server != nil {
		return sw.Server.Shutdown(ctx)
	}

	return errors.New("server is nil while trying to shutdown")
}
