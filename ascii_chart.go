package pnutmux

import (
	"fmt"
	"regexp"
	"sort"

	"github.com/fatih/color"
	"github.com/mattn/go-runewidth"
	"gitlab.com/fruitygo/pnutmux/pnutconfig"
)

// Define informative constants.
var (
	// version = yellow(pnutconfig.V)
	gitURL = "gitlab.com/fruitygo/pnutmux"
	thanks = yellow("Thanks For Using 🥜 Pnutmux")
)

// Define ascii art.
var (
	asciiLines = []string{
		" ____  _  _  __  __  ____  __  __  __  __  _  _ ",
		"(  _ \\( \\( )(  )(  )(_  _)(  \\/  )(  )(  )( \\/ )",
		" )___/ )  (  )(__)(   )(   )    (  )(__)(  )  ( ",
		"(__)  (_)\\_)(______) (__) (_/\\/\\_)(______)(_/\\_)",
	}
)

// General UI constants.
const (
	side      = "|"
	topBottom = "_"
	jump      = "\n"
	pad       = " "
	vSpacer   = ""
	tab       = "    "
	widthIncr = 5
	padding   = 11
)

// General color constants.
var (
	width  = 5
	green  = color.New(color.FgGreen).SprintFunc()
	yellow = color.New(color.FgHiYellow).SprintFunc()
	red    = color.New(color.FgRed).SprintFunc()
)

// String alias.
type chartKey = string

// Define chart keys.
const (
	h2cKey                    chartKey = "h2c"
	http2Key                  chartKey = "http2"
	servehttp                 chartKey = "servehttp"
	rateLimiting              chartKey = "rateLimiting"
	rateLimitingMode          chartKey = "rateLimitingMode"
	rateLimitingHeaderKey     chartKey = "rateLimitingHeaderKey"
	rateLimitingMaxDailyScore chartKey = "rateLimitingMaxDailyScore"
	rateLimitingTimeout       chartKey = "rateLimitingTimeout"
	defaultStructuredLogger   chartKey = "defaultStructuredLogger"
	defaultCustomHandler      chartKey = "defaultCustomHandler"
	boundOn                   chartKey = "boundOn"
	idleTimeout               chartKey = "idleTimeout"
	readTimeout               chartKey = "readTimeout"
	writeTimeout              chartKey = "writeTimeout"
	routesRegistered          chartKey = "routesRegistered"
	handlersRegistered        chartKey = "handlersRegistered"
)

// generateContent generates a formatted string based on the provided parameters.
func generateContent(condition bool, value interface{}, negativeValue interface{}) string {
	if condition {
		return green(fmt.Sprintf("%v", value))
	}

	return red(fmt.Sprintf("%v", negativeValue))
}

// Define the print order of the chart middle section.
var printOrderMidSection = []chartKey{
	boundOn,
	idleTimeout,
	readTimeout,
	writeTimeout,
	rateLimiting,
	rateLimitingMode,
	rateLimitingHeaderKey,
	rateLimitingTimeout,
	rateLimitingMaxDailyScore,
	servehttp,
	defaultStructuredLogger,
	defaultCustomHandler,
	http2Key,
	h2cKey,
	routesRegistered,
	handlersRegistered,
}

// routeInfo holds the information for a route.
type routeInfo struct {
	Reg   *regexp.Regexp
	Route *pnutconfig.Route
}

// printRouterState prints the router state.
func (r *PnutRouter) printRouterState() {
	// Lock mutex.
	r.pnutRouterMu.RLock()
	defer r.pnutRouterMu.RUnlock()

	// Assert server to serverwrapper.
	sw, ok := r.srv.(*serverwrapper)
	if !ok {
		return
	}

	// Define the top content.
	topContent := []string{
		asciiLines[0],
		asciiLines[1],
		asciiLines[2],
		asciiLines[3],
		vSpacer,
		// version,
		vSpacer,
		gitURL,
		thanks,
	}

	// Define the middle content.
	midContentKeys := map[chartKey]string{
		servehttp:                 "ServeHTTP Semaphore Count",
		defaultCustomHandler:      "Default Not Found Handler",
		http2Key:                  "HTTP/2",
		h2cKey:                    "Clear Text HTTP/2",
		defaultStructuredLogger:   "Default Structured Logger",
		rateLimiting:              "Rate Limiting",
		rateLimitingMode:          "Rate Limiting Mode",
		rateLimitingHeaderKey:     "Rate Limiting Header Key",
		rateLimitingMaxDailyScore: "Rate Limiting Max Daily Score",
		rateLimitingTimeout:       "Rate Limiting Timeout",
		boundOn:                   "Bound On",
		idleTimeout:               "Idle Timeout",
		readTimeout:               "Read Timeout",
		writeTimeout:              "Write Timeout",
		routesRegistered:          fmt.Sprint(pluralize("Route", len(r.routes)) + " Registered"),
		handlersRegistered:        fmt.Sprint(pluralize("Handler", len(r.routes)) + " Registered"),
	}

	// Define the total handler number.
	totalHandlers := 0
	for _, route := range r.routes {
		totalHandlers += len(route.Handlers)
	}

	// Define bottom content.
	bottomContent := []string{
		yellow("🥜 Pnutmux Registry"),
		vSpacer,
	}

	// Define variables.
	var maxDailyScore, timeout, mode, headerKey string

	// Define limiting variables if limiting is enabled.
	if r.limiter != nil {
		maxDailyScore = fmt.Sprintf("%d", r.limiter.GetMaxDailyScore())
		timeout = r.limiter.GetTimeout()
		mode = r.limiter.GetRateLimitingMode()
		headerKey = r.limiter.GetHeaderKey()
	}

	// Define limiting chart entries.
	maxDailyScore = generateContent((r.limiter != nil), maxDailyScore, "Not Applicable")
	timeout = generateContent((r.limiter != nil), timeout, "Not Applicable")
	mode = generateContent((r.limiter != nil), mode, "Not Applicable")
	headerKey = generateContent((r.limiter != nil), headerKey, "Not Applicable")

	// Define middle content.
	midContentValues := map[chartKey]string{
		servehttp:                 green(fmt.Sprintf("%d", r.maxConcurrent)),
		rateLimiting:              generateContent(r.limiter != nil, "ON", "OFF"),
		rateLimitingMode:          mode,
		rateLimitingHeaderKey:     headerKey,
		rateLimitingMaxDailyScore: maxDailyScore,
		rateLimitingTimeout:       timeout,
		http2Key:                  generateContent(r.http2Config != nil && r.http2Config.IsValid(), "ON", "OFF"),
		h2cKey:                    generateContent(r.http2Config != nil && r.http2Config.UseH2C, "ON", "OFF"),
		defaultStructuredLogger:   generateContent(r.defaultLogger, "ON", "OFF"),
		defaultCustomHandler:      generateContent(r.defaultNotFound, "ON", "OFF"),
		boundOn:                   green(sw.Addr),
		idleTimeout:               green(fmt.Sprintf("%v", sw.IdleTimeout)),
		readTimeout:               green(fmt.Sprintf("%v", sw.ReadTimeout)),
		writeTimeout:              green(fmt.Sprintf("%v", sw.WriteTimeout)),
		routesRegistered:          green(fmt.Sprintf("%d", len(r.routes))),
		handlersRegistered:        green(fmt.Sprintf("%d", totalHandlers)),
	}

	// Increase width until it is enough to print the chart.
	for {
		counter := 0
		widthIsEnough := true

		// Check width for top content
		for i := 0; i < len(topContent); i++ {
			if visibleLength(topContent[i]) > width-padding {
				widthIsEnough = false

				break
			}
		}

		// Check width for middle content.
		if widthIsEnough {
			for _, key := range printOrderMidSection {
				value := midContentValues[key]

				if visibleLength(midContentKeys[key])+visibleLength(value)+padding > width {
					widthIsEnough = false

					break
				}
			}
		}

		// Check width for bottom content.
		if widthIsEnough {
			for i := 0; i < len(bottomContent); i++ {
				if visibleLength(bottomContent[i]) > width-padding {
					widthIsEnough = false

					break
				}
			}
		}

		// Check width for routes.
		if widthIsEnough {
			for reg, route := range r.routes {
				counter++
				routestr := fmt.Sprintf("• Route %d: %s, Handlers: %d", counter, reg.String(), len(route.Handlers))

				if visibleLength(routestr) > width-padding {
					widthIsEnough = false
					break
				}

				// Check details of each handler
				for method := range route.Handlers {
					weight := route.Weights[method]
					handlerStr := fmt.Sprintf(tab+"• Method: %s → Weight: %d", method, weight)
					if visibleLength(handlerStr) > width-padding {
						widthIsEnough = false
						break
					}
				}

				if !widthIsEnough {
					break
				}
			}
		}

		if widthIsEnough {
			break
		}

		width += widthIncr
	}

	// Print top of the chart.
	printTop()

	// Print top entries.
	for i := 0; i < len(topContent); i++ {
		fmt.Print(side)

		if i < len(topContent) {
			leftPadding := (width-visibleLength(topContent[i]))/2 - 1

			rightPadding := leftPadding

			if (width-visibleLength(topContent[i]))%2 != 0 {
				rightPadding++
			}

			for j := 0; j < leftPadding; j++ {
				fmt.Print(pad)
			}

			fmt.Print(topContent[i])

			for j := 0; j < rightPadding; j++ {
				fmt.Print(pad)
			}
		}

		fmt.Print(side + jump)
	}

	// Print last row of top section.
	printLastRow()

	// Middle section.
	for _, key := range printOrderMidSection {
		value := midContentValues[key]

		fmt.Print(side)

		dotPadding := width - visibleLength(midContentKeys[key]) - visibleLength(value) - 2

		fmt.Print(midContentKeys[key])

		for j := 0; j < dotPadding; j++ {
			fmt.Print(".")
		}

		fmt.Print(value)

		fmt.Print(side + jump)
	}

	// Print last row of middle section.
	printLastRow()

	for i := 0; i < len(bottomContent); i++ {

		fmt.Print(side)

		if i < len(bottomContent) {
			leftPadding := (width-visibleLength(bottomContent[i]))/2 - 1

			rightPadding := leftPadding

			if (width-visibleLength(bottomContent[i]))%2 != 0 {
				rightPadding++
			}

			for j := 0; j < leftPadding; j++ {
				fmt.Print(pad)
			}

			fmt.Print(bottomContent[i])

			for j := 0; j < rightPadding; j++ {
				fmt.Print(pad)
			}
		}

		fmt.Print(side + jump)
	}

	// Define bottom section informative slice.
	var routeInfoSlice []routeInfo

	// Fill the slice.
	for reg, route := range r.routes {
		routeInfoSlice = append(routeInfoSlice, routeInfo{Reg: reg, Route: route})
	}

	// Sort the slice by length of regex.
	sort.Slice(routeInfoSlice, func(i, j int) bool {
		return len(routeInfoSlice[i].Reg.String()) < len(routeInfoSlice[j].Reg.String())
	})

	// Define a conter.
	counter := 0

	// Iterate informative slice.
	for _, routeInfo := range routeInfoSlice {
		// Increment the counter.
		counter++

		fmt.Print(side)

		routestr := fmt.Sprintf("🥜 Route %d: %s, Handlers: %d", counter, routeInfo.Reg.String(), len(routeInfo.Route.Handlers))

		fmt.Print(routestr)

		for j := visibleLength(routestr); j < width-2; j++ {
			fmt.Print(pad)
		}

		fmt.Print(side + jump)

		// Print details of each handler.
		for method := range routeInfo.Route.Handlers {
			weight := routeInfo.Route.Weights[method]

			fmt.Print(side)

			handlerStr := fmt.Sprintf(tab+"• Method: %s → Weight: %d", method, weight)

			fmt.Print(handlerStr)

			for j := visibleLength(handlerStr); j < width-2; j++ {
				fmt.Print(pad)
			}

			fmt.Print(side + jump)
		}
	}

	// Print last row of bottom section.
	printLastRow()
}

// printTop prints top of the chart.
func printTop() {
	for i := 0; i < width; i++ {
		switch i {
		case 0:
			fmt.Print(pad)
		case width - 1:
			fmt.Print(pad + jump)
		default:
			fmt.Print(topBottom)
		}
	}
}

// printLastRow prints last row of the chart sections.
func printLastRow() {
	for i := 0; i < width; i++ {
		switch i {
		case 0:
			fmt.Print(side)
		case width - 1:
			fmt.Print(side + jump)
		default:
			fmt.Print(topBottom)
		}
	}
}

// visibleLength returns the visible length of a string.
func visibleLength(s string) int {
	re := regexp.MustCompile("\x1b[^m]*m")

	visibleString := re.ReplaceAllString(s, "")

	return runewidth.StringWidth(visibleString)
}

// pluralize returns a pluralized word.
func pluralize(word string, count int) string {
	if count > 1 {
		return word + "s"
	}

	return word
}
